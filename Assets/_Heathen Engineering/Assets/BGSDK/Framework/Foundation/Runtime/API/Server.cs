﻿#if UNITY_SERVER || UNITY_EDITOR || UNITY_ANDROID
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using HeathenEngineering.BGSDK.DataModel;
using HeathenEngineering.BGSDK.Engine;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Events;
using WalletConnectSharp.Unity;
using WalletConnectSharp.Core;

namespace HeathenEngineering.BGSDK.API
{
    public static class Server
    {
        
        
        /// <summary>
        /// A wrapper around BGSDK's Token Management API
        /// </summary>
        /// <remarks>
        /// <para>
        /// For more detrails see <see href="https://docs.venly.io/pages/token-management.html#_token_management">https://docs.venly.io/pages/token-management.html#_token_management</see>.
        /// All functions of this class and child classes are designed to be used with Unity's StartCoroutine method.
        /// All funcitons of this class will take an Action as the final paramiter which is called when the process completes.
        /// Actions can be defined as a funciton in the calling script or can be passed as an expression.
        /// </para>
        /// <code>
        /// StartCoroutine(API.Tokens.GetContract(Identity, contract, HandleGetContractResults));
        /// </code>
        /// <para>
        /// or
        /// </para>
        /// <code>
        /// StartCoroutine(API.Tokens.GetContract(Identity, contract, (resultObject) => 
        /// {
        ///     //TODO: handle the resultObject
        /// }));
        /// </code>
        /// <para>
        /// Additional code samples can be found in the Samples provided with the package.
        /// </para>
        /// </remarks>
        public static class Tokens
        {
            /// <summary>
            /// Fetch details about a specific contract for the current app Id
            /// </summary>
            /// <param name="contract">The contract to get</param>
            /// <param name="callback">The method to call back into with the results.</param>
            /// <returns>The Unity routine enumerator</returns>
            /// <remarks>
            /// <para>
            /// For more information please see <see href="https://docs.venly.io/pages/token-management.html#_get_contract">https://docs.venly.io/pages/token-management.html</see>
            /// </para>
            /// </remarks>
            /// <example>
            /// <para>
            /// How to call:
            /// </para>
            /// <code>
            /// StartCoroutine(API.Tokens.GetContract(Identity, contract, HandleGetContractResults));
            /// </code>
            /// </example>
            public static IEnumerator GetContract(Engine.Contract contract, Action<ContractResult> callback)
            {
                if (BGSDKSettings.current == null)
                {
                    callback(new ContractResult()
                    {
                        hasError = true,
                        message = "Attempted to call BGSDK.Tokens.GetContract with no BGSDK.Settings object applied.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    if (BGSDKSettings.user == null)
                    {
                        callback(new ContractResult()
                        {
                            hasError = true,
                            message =
                                "BGSDKIdentity required, null identity provided.\nPlease initalize the Settings.user variable before calling GetContract",
                            result = null
                        });
                        yield return null;
                    }
                    else
                    {
                        UnityWebRequest www =
                            UnityWebRequest.Get(BGSDKSettings.current.ContractUri + "/" + contract.Id);
                        www.SetRequestHeader("Authorization",
                            BGSDKSettings.user.authentication.token_type + " " +
                            BGSDKSettings.user.authentication.access_token);

                        var co = www.SendWebRequest();
                        while (!co.isDone)
                            yield return null;

                        if (!www.isNetworkError && !www.isHttpError)
                        {
                            string resultContent = www.downloadHandler.text;
                            var results = new ContractResult();
                            results.result = JsonUtility
                                .FromJson<ListContractsResult.RecieveContractModel>(resultContent)?.ToContractData();
                            results.message = "Get Contract complete.";
                            results.httpCode = www.responseCode;
                            callback(results);
                        }
                        else
                        {
                            callback(new ContractResult()
                            {
                                hasError = true,
                                message = "Error:" + (www.isNetworkError
                                    ? " a network error occured while requesting the contract."
                                    : " a HTTP error occured while requesting the contract."),
                                result = null, httpCode = www.responseCode
                            });
                        }
                    }
                }
            }

            /// <summary>
            /// <para>Returns the list of available token types for the indicated contract</para>
            /// <see href="https://docs.venly.io/pages/token-management.html">https://docs.venly.io/pages/token-management.html</see>
            /// </summary>
            /// <param name="contract">The contract to get</param>
            /// <param name="callback">The method to call back into with the results.</param>
            /// <returns>The Unity routine enumerator</returns>
            /// <example>
            /// <para>
            /// How to call:
            /// </para>
            /// <code>
            /// StartCoroutine(API.Tokens.ListTokenTypes(Identity, contract, HandleListTokenTypeResults));
            /// </code>
            /// </example>
            public static IEnumerator ListTokenTypes(Contract contract, Action<ListTokenTypesResult> callback)
            {
                if (BGSDKSettings.current == null)
                {
                    callback(new ListTokenTypesResult()
                    {
                        hasError = true,
                        message =
                            "Attempted to call BGSDK.Tokens.ListTokenTypes with no BGSDK.Settings object applied.",
                        result = null
                    });
                    yield return null;
                }
                else if (BGSDKSettings.user == null)
                {
                    callback(new ListTokenTypesResult()
                    {
                        hasError = true,
                        message =
                            "BGSDKIdentity required, null identity provided.\nPlease initalize the Settings.user variable before calling ListTokenTypes",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www = UnityWebRequest.Get(BGSDKSettings.current.GetTokenUri(contract));
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        string resultContent = www.downloadHandler.text;
                        var results = new ListTokenTypesResult();
                        results = JsonUtility.FromJson<ListTokenTypesResult>(Utilities.JSONArrayWrapper(resultContent));
                        results.message = "List Token Types complete.";
                        results.httpCode = www.responseCode;
                        callback(results);
                    }
                    else
                    {
                        callback(new ListTokenTypesResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting the list of available token types."
                                : " a HTTP error occured while requesting the list of availabel token types."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }

            /// <summary>
            /// <para>Returns the definition of the indicated token</para>
            /// <see href="https://docs.venly.io/pages/token-management.html">https://docs.venly.io/pages/token-management.html</see>
            /// </summary>
            /// <param name="contract">The contract to get</param>
            /// <param name="tokenId">The id of the token to fetch</param>
            /// <param name="callback">The method to call back into with the results.</param>
            /// <returns>The Unity routine enumerator</returns>
            /// <example>
            /// <para>
            /// How to call:
            /// </para>
            /// <code>
            /// StartCoroutine(API.Tokens.GetTokenType(Identity, contract, tokenId, HandleGetTokenTypeResults));
            /// </code>
            /// </example>
            public static IEnumerator GetTokenType(Contract contract, string tokenId,
                Action<DataModel.DefineTokenTypeResult> callback)
            {
                if (BGSDKSettings.current == null)
                {
                    callback(new DataModel.DefineTokenTypeResult()
                    {
                        hasError = true,
                        message = "Attempted to call BGSDK.Tokens.GetTokenType with no BGSDK.Settings object applied.",
                        result = null
                    });
                    yield return null;
                }
                else if (BGSDKSettings.user == null)
                {
                    callback(new DefineTokenTypeResult()
                    {
                        hasError = true,
                        message =
                            "BGSDKIdentity required, null identity provided.\nPlease initalize the Settings.user variable before calling GetTokenType",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www =
                        UnityWebRequest.Get(BGSDKSettings.current.GetTokenUri(contract) + "/" + tokenId);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        string resultContent = www.downloadHandler.text;
                        var results = new DataModel.DefineTokenTypeResult();
                        results.result = JsonUtility.FromJson<DataModel.TokenResponceData>(resultContent);
                        results.message = "List Token Types complete.";
                        results.httpCode = www.responseCode;
                        callback(results);
                    }
                    else
                    {
                        callback(new DataModel.DefineTokenTypeResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting the definition of token " + tokenId + "."
                                : " a HTTP error occured while requesting the definition of token " + tokenId + "."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }



            /// <summary>
            /// Returns tokens for given token type
            /// </summary>
            /// <param name="token">The token type to query</param>
            /// <param name="callback">The callback to invoke with the results</param>
            /// <returns></returns>
            public static IEnumerator GetTokens(Token token, Action<Token.ResultList> callback) => token.Get(callback);

            //----------------------------------------------------------------------------------------------------------
            public static IEnumerator CreateTokenType(Token token, Contract contract, Action<DefineTokenTypeResult> callback)
            {

                if (BGSDKSettings.current == null)
                {
                    callback(new DefineTokenTypeResult()
                    {
                        hasError = true,
                        message =
                            "Attempted to call BGSDK.Tokens.CreateTokenType with no BGSDK.Settings object applied."
                    });
                    yield return null;
                }
                else
                {

                    if (BGSDKSettings.user == null)
                    {
                        callback(new DefineTokenTypeResult()
                        {
                            hasError = true,
                            message =
                                "BGSDKSettings.user required, null Settings.user provided.\n Please initalize the Settings.user before calling CreateTokenType"
                        });
                        yield return null;
                    }
                    else
                    {
                        int attackDamage = 0;
                        int defense = 0;
                        int farmPower = 0;
                        int level = 0;
                        string parent = null;
                        var _a = token.GetTokenDefinition();
                        foreach (var attr in _a.attributes)
                        {
                            switch (attr.name)
                            {
                                case "Attack Damage":
                                    attackDamage = int.Parse(attr.value);
                                    break;
                                case "Defense":
                                    defense = int.Parse(attr.value);
                                    break;
                                case "Farm Power":
                                    farmPower = int.Parse(attr.value);
                                    break;
                                case "Level":
                                    level = int.Parse(attr.value);
                                    break;
                                case "Parent":
                                    parent = attr.value;
                                    break;
                            }
                        }



                        CreateTokenTypeRequest farm_type = new CreateTokenTypeRequest
                        {
                            name = token.SystemName,
                            description = token.Description,
                            backgroundColor = "#eeeeee",
                            fungible = false,
                            burnable = true,
                            externalUrl =
                                "https://bitbucket.org/rabbi062/nft-project/raw/1a641e6d7c0b86eff78873fcabdeb2528239d9d6/Assets/GUI%20PRO%20Kit%20-%20Fantasy%20RPG/ResourcesData/Sprites/Component/Icon_EquipmentIcons_(Original)/equip_hammer_4.png",
                            image = token.Image,
                            maxSupply = 1.ToString(),
                            
                            attributes = new List<CreateTokenTypeRequest.Attribute>()
                            {
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Farm Power",
                                    value = farmPower.ToString(),
                                    maxValue = 100.ToString()
                                },
                            
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Level",
                                    value = level.ToString(),
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Type",
                                    value = "FarmWeapon",
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Child",
                                    value = parent + "01",
                                    maxValue = 100.ToString()
                                }
                            
                            }
                        };

                        CreateTokenTypeRequest weapon_type = new CreateTokenTypeRequest
                        {
                            name = token.SystemName,
                            description = token.Description,
                            backgroundColor = "#eeeeee",
                            fungible = false,
                            burnable = true,
                            externalUrl =
                                "https://bitbucket.org/rabbi062/nft-project/raw/1a641e6d7c0b86eff78873fcabdeb2528239d9d6/Assets/GUI%20PRO%20Kit%20-%20Fantasy%20RPG/ResourcesData/Sprites/Component/Icon_EquipmentIcons_(Original)/equip_axe_1.png",
                            image = token.Image,
                            maxSupply = 1.ToString(),
                            attributes = new List<CreateTokenTypeRequest.Attribute>()
                            {
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Attack Damage",
                                    value = attackDamage.ToString(),
                                    maxValue = 100.ToString()
                                },
                                
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Defense",
                                    value = defense.ToString(),
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Level",
                                    value = level.ToString(),
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Type",
                                    value = "ActionWeapon",
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Child",
                                    value = parent + "01",
                                    maxValue = 100.ToString()
                                }
                            }

                        };
                        


                    //now use this def to body
                    //TokenProperties<string> str_prop = new TokenProperties<string>();
                    string jsonString = null;
                    var a = token.GetTokenDefinition();
                    foreach (var attr in a.attributes)
                    {
                        switch (attr.name)
                        {
                            case "Attack Damage":
                                jsonString = JsonUtility.ToJson(weapon_type);
                                break;
                            case "Farm Power":
                                jsonString = JsonUtility.ToJson(farm_type);
                                break;
                        }
                    }

                    Debug.Log(jsonString);
                    
                    
                    UnityWebRequest www = UnityWebRequest.Put("https://api-business-staging.arkane.network/api/apps/ec9392e6-2c69-4266-87c3-b9ff4adc377f/contracts/2339/token-types", jsonString);
                    www.method = UnityWebRequest.kHttpVerbPOST;
                    www.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
                    www.uploadHandler.contentType = "application/json;charset=UTF-8";
                    www.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    
                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;
                    
                    if (!www.isNetworkError && !www.isHttpError)
                    {
                    
                        //WebResults<TokenResponceData> results = new WebResults<TokenResponceData>(www);
                        var results = new DefineTokenTypeResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            TokenDefinition def = JsonUtility.FromJson<TokenDefinition>(resultContent);
                            results.result = (TokenResponceData)JsonUtility.FromJson<TokenResponceData>(resultContent);
                            results.result.contractAddress = contract.data.address;
                            results.result.contractTypeId = new System.Numerics.BigInteger(int.Parse(contract.data.id));// ;
                            
                            TokenDefinition result_def = results.result as TokenDefinition;
                            
                            if(result_def != null)
                            {
                                result_def = def;
                            }
                            results.message = "SUCCESS";
                            results.success = true;
                            results.httpCode = www.responseCode;
                            //Debug.Log(resultContent);
                            //Info = resultContent;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    
                    }
                    else
                    {
                        var results = new DefineTokenTypeResult();
                        results.message =
                            "ERROR";
                        callback(results);
                    }

                }


            }

        }

            //----------------------------------------------------------------------------------------------------------
            public static IEnumerator UpdateTokenTypeMetadata(Token token, Contract contract, Action<UpdateTokenMetadataResult> callback)
            {
                if (BGSDKSettings.current == null)
                {
                    callback(new UpdateTokenMetadataResult()
                    {
                        hasError = true,
                        message =
                            "Attempted to call BGSDK.Tokens.Update TokenType with no BGSDK.Settings object applied."
                    });
                    yield return null;
                }
                else
                {
                    if (BGSDKSettings.user == null)
                    {
                        callback(new UpdateTokenMetadataResult()
                        {
                            hasError = true,
                            message =
                                "BGSDKSettings.user required, null Settings.user provided.\n Please initalize the Settings.user before calling CreateTokenType"
                        });
                        yield return null;
                    }
                    else
                    {
                        
                        
                        int attackDamage = 0;
                        int defense = 0;
                        int farmPower = 0;
                        int level = 0;
                        string parent = null;
                        string tokenTypeId = null;
                        var _a = token.GetTokenDefinition();
                        foreach (var attr in _a.attributes)
                        {
                            switch (attr.name)
                            {
                                case "Attack Damage":
                                    attackDamage = int.Parse(attr.value);
                                    break;
                                case "Defense":
                                    defense = int.Parse(attr.value);
                                    break;
                                case "Farm Power":
                                    farmPower = int.Parse(attr.value);
                                    break;
                                case "Level":
                                    level = int.Parse(attr.value);
                                    break;
                                case "Parent":
                                    parent = attr.value;
                                    break;
                                case "tokenTypeId":
                                    tokenTypeId = attr.value;
                                    break;
                            }
                        }



                        CreateTokenTypeRequest farm_type = new CreateTokenTypeRequest
                        {
                            name = token.SystemName,
                            description = token.Description,
                            backgroundColor = "#eeeeee",
                            fungible = false,
                            burnable = true,
                            externalUrl =
                                "https://bitbucket.org/rabbi062/nft-project/raw/1a641e6d7c0b86eff78873fcabdeb2528239d9d6/Assets/GUI%20PRO%20Kit%20-%20Fantasy%20RPG/ResourcesData/Sprites/Component/Icon_EquipmentIcons_(Original)/equip_hammer_4.png",
                            image = token.Image,
                            maxSupply = 1.ToString(),
                            
                            attributes = new List<CreateTokenTypeRequest.Attribute>()
                            {
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Farm Power",
                                    value = farmPower.ToString(),
                                    maxValue = 100.ToString()
                                },
                            
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Level",
                                    value = level.ToString(),
                                    maxValue = 100.ToString()
                                },
                                
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Parent",
                                    value = parent,
                                    maxValue = 100.ToString()
                                },
                                
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Child Count",
                                    value = 1.ToString(),
                                    maxValue = 100.ToString()
                                }
                            
                            }
                        };

                        CreateTokenTypeRequest weapon_type = new CreateTokenTypeRequest
                        {
                            name = token.SystemName,
                            description = token.Description,
                            backgroundColor = "#eeeeee",
                            fungible = false,
                            burnable = true,
                            externalUrl =
                                "https://bitbucket.org/rabbi062/nft-project/raw/1a641e6d7c0b86eff78873fcabdeb2528239d9d6/Assets/GUI%20PRO%20Kit%20-%20Fantasy%20RPG/ResourcesData/Sprites/Component/Icon_EquipmentIcons_(Original)/equip_axe_1.png",
                            image = token.Image,
                            maxSupply = 1.ToString(),
                            attributes = new List<CreateTokenTypeRequest.Attribute>()
                            {
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Attack Damage",
                                    value = attackDamage.ToString(),
                                    maxValue = 100.ToString()
                                },
                                
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Defense",
                                    value = defense.ToString(),
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Level",
                                    value = level.ToString(),
                                    maxValue = 100.ToString()
                                },
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Parent",
                                    value = parent,
                                    maxValue = 100.ToString()
                                },
                                
                                new CreateTokenTypeRequest.Attribute()
                                {
                                    type = "property",
                                    name = "Child Count",
                                    value = 1.ToString(),
                                    maxValue = 100.ToString()
                                }
                            }

                        };
                        


                    //now use this def to body
                    //TokenProperties<string> str_prop = new TokenProperties<string>();
                    string jsonString = null;
                    var a = token.GetTokenDefinition();
                    foreach (var attr in a.attributes)
                    {
                        switch (attr.name)
                        {
                            case "Attack Damage":
                                jsonString = JsonUtility.ToJson(weapon_type);
                                break;
                            case "Farm Power":
                                jsonString = JsonUtility.ToJson(farm_type);
                                break;
                        }
                    }

                    
                        
                        UnityWebRequest www = UnityWebRequest.Post("https://api-business-staging.arkane.network/api/apps/ec9392e6-2c69-4266-87c3-b9ff4adc377f/contracts/2339/token-types/"+ tokenTypeId +"/metadata", jsonString);
                        www.method = UnityWebRequest.kHttpVerbPUT;
                        www.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
                        www.uploadHandler.contentType = "application/json;charset=UTF-8";
                        www.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");

                        var co = www.SendWebRequest();
                        while (!co.isDone)
                            yield return null;
                        if (!www.isNetworkError && !www.isHttpError)
                    {

                        //WebResults<TokenResponceData> results = new WebResults<TokenResponceData>(www);
                        var results = new UpdateTokenMetadataResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<UpdateTokenMetadataResult>(resultContent);
                            results.message = "SUCCESS";
                            results.success = true;
                            results.httpCode = www.responseCode;
                            //Debug.Log(resultContent);
                            //Info = resultContent;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }

                    }
                    else
                    {
                        var results = new UpdateTokenMetadataResult();
                        results.message =
                            "ERROR";
                        callback(results);
                    }
                        
                    }
                }
                
                
                
                
                
            }
            

        }

        /// <summary>
    /// Wraps the BGSDK interface for wallets incuding User, App and Whitelable wallets.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Wallet funcitonality is discribed in the <see href="https://docs.venly.io/pages/reference.html">https://docs.venly.io/pages/reference.html</see> documentation.
    /// All functions of this class and child classes are designed to be used with Unity's StartCoroutine method.
    /// All funcitons of this class will take an Action as the final paramiter which is called when the process completes.
    /// Actions can be defined as a funciton in the calling script or can be passed as an expression.
    /// </para>
    /// <code>
    /// StartCoroutine(API.Wallets.Get(Settings.user, walletId, HandleResults));
    /// </code>
    /// <para>
    /// or
    /// </para>
    /// <code>
    /// StartCoroutine(API.Wallets.Get(Settings.user, walletId, (resultObject) => 
    /// {
    ///     //TODO: handle the resultObject
    /// }));
    /// </code>
    /// <para>
    /// Additional code samples can be found in the Samples provided with the package.
    /// </para>
    /// </remarks>
        public static class Wallets
    {
        [HideInInspector]
        public class CreateWalletModel
        {
            public string walletType;
            public string secretType;
            public string identifier;
            public string pincode;
        }

        public enum Type
        {
            WHITE_LABEL,
            UNRECOVERABLE_WHITE_LABEL
        }

        /// <summary>
        /// Create a new white label style wallet for the user
        /// </summary>
        /// <remarks>
        /// See <see href="https://docs.venly.io/api/api-products/wallet-api/create-wallet"/> for more details
        /// </remarks>
        /// <param name="pincode">[Required] The pin that will encrypt and decrypt the wallet</param>
        /// <param name="identifier">[Optional] An identifier that can be used to query or group wallets</param>
        /// <param name="description">[Optional] A description to describe the wallet.</param>
        /// <param name="chain">The blockchain on which to create the wallet</param>
        /// <param name="type">Define if the wallet is recoverable or unrecoverable</param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static IEnumerator Create(string pincode, string identifier, SecretType chain,
            Action<ListWalletResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new ListWalletResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Wallets.CreateWhitelabelWallet with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {

                if (BGSDKSettings.user == null)
                {
                    callback(new ListWalletResult()
                    {
                        hasError = true,
                        message =
                            "BGSDKSettings.user required, null Settings.user provided.\n Please initalize the Settings.user before calling CreateWhitelableWallet",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    var walletModel = new CreateWalletModel
                    {
                        walletType = "WHITE_LABEL",
                        identifier = identifier,
                        pincode = pincode,
                    };

                    switch (chain)
                    {
                        case SecretType.AVAC:
                            walletModel.secretType = "AVAC";
                            break;
                        case SecretType.BSC:
                            walletModel.secretType = "BSC";
                            break;
                        case SecretType.ETHEREUM:
                            walletModel.secretType = "ETHEREUM";
                            break;
                        case SecretType.MATIC:
                            walletModel.secretType = "MATIC";
                            break;
                    }

                    var jsonString = JsonUtility.ToJson(walletModel);

                    UnityWebRequest www = UnityWebRequest.Put(BGSDKSettings.current.WalletUri, jsonString);
                    www.method = UnityWebRequest.kHttpVerbPOST;
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);
                    www.uploadHandler.contentType = "application/json;charset=UTF-8";
                    www.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new ListWalletResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results.result = new System.Collections.Generic.List<Wallet>();
                            results.result.Add(JsonUtility.FromJson<Wallet>(Utilities.JSONArrayWrapper(resultContent)));
                            results.message = "Create wallet complete.";
                            results.httpCode = www.responseCode;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new ListWalletResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while attempting creat wallet."
                                : " a HTTP error occured while attempting to creat wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Gets the user wallets available to the authorized Settings.user
        /// </summary>
        /// <param name="callback">A method pointer to handle the results of the query</param>
        /// <returns>The Unity routine enumerator</returns>
        /// <remarks>
        /// <see href="https://docs.venly.io/pages/reference.html#_list_wallets_arkane_api">https://docs.venly.io/pages/reference.html#_list_wallets_arkane_api</see>
        /// </remarks>
        public static IEnumerator List(Action<ListWalletResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new ListWalletResult()
                {
                    hasError = true,
                    message = "Attempted to call BGSDK.Wallets.List with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new ListWalletResult()
                    {
                        hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www = UnityWebRequest.Get(BGSDKSettings.current.WalletUri);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new ListWalletResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<ListWalletResult>(Utilities.JSONArrayWrapper(resultContent));
                            results.message = "Wallet refresh complete.";
                            results.httpCode = www.responseCode;

                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new ListWalletResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting the user's wallets."
                                : " a HTTP error occured while requesting the user's wallets."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Gets a user wallet as available to the authorized Settings.user
        /// </summary>
        /// <param name="Settings.user">The Settings.user to query for</param>
        /// <param name="callback">A method pointer to handle the results of the query</param>
        /// <returns>The Unity routine enumerator</returns>
        /// <remarks>
        /// <see href="https://docs.venly.io/pages/reference.html#get-specific-user-wallet">https://docs.venly.io/pages/reference.html#get-specific-user-wallet</see>
        /// </remarks>
        public static IEnumerator Get(string walletId, Action<ListWalletResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new ListWalletResult()
                {
                    hasError = true,
                    message = "Attempted to call BGSDK.Wallets.UserWallet.Get with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new ListWalletResult()
                    {
                        hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www = UnityWebRequest.Get(BGSDKSettings.current.WalletUri + "/" + walletId);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new ListWalletResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results.result = new System.Collections.Generic.List<Wallet>();
                            results.result.Add(JsonUtility.FromJson<Wallet>(resultContent));
                            results.message = "Wallet refresh complete.";
                            results.httpCode = www.responseCode;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new ListWalletResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting a user's wallet."
                                : " a HTTP error occured while requesting a user's wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }



        public static IEnumerator GetByIdentifier(string walletIdentifier, Action<GetWalletByIdentifierResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new GetWalletByIdentifierResult()
                {
                    hasError = true,
                    message = "Attempted to call BGSDK.Wallets.UserWallet.Get with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new GetWalletByIdentifierResult()
                        {hasError = true, message = "BGSDKSettings.user required, null Settings.user provided."});
                    yield return null;
                }
                else
                {
                    UnityWebRequest www =
                        UnityWebRequest.Get("https://api-staging.arkane.network/api/wallets?identifier=" +
                                            walletIdentifier);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new GetWalletByIdentifierResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<GetWalletByIdentifierResult>(resultContent);
                            results.message = "Wallet refresh complete.";
                            results.httpCode = www.responseCode;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new GetWalletByIdentifierResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting a user's wallet."
                                : " a HTTP error occured while requesting a user's wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }



        /// <summary>
        /// Endpoint that allows updating the details of a wallet (ex. pincode).
        /// </summary>
        /// <remarks>
        /// <para>
        /// For more information please see <see href="https://docs-staging.venly.io/pages/whitelabel.html#_update_wallet_arkane_api">https://docs-staging.venly.io/pages/whitelabel.html#_update_wallet_arkane_api</see>
        /// </para>
        /// </remarks>
        /// <param name="walletId"></param>
        /// <param name="currentPincode"></param>
        /// <param name="newPincode"></param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator UpdatePincode(string walletId, string currentPincode, string newPincode,
            Action<ListWalletResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new ListWalletResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Wallets.CreateWhitelabelWallet with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {

                if (BGSDKSettings.user == null)
                {
                    callback(new ListWalletResult()
                    {
                        hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    WWWForm form = new WWWForm();
                    form.AddField("pincode", currentPincode);
                    form.AddField("newPincode", newPincode);

                    UnityWebRequest www =
                        UnityWebRequest.Post(BGSDKSettings.current.WalletUri + "/" + walletId + "/security", form);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new ListWalletResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results.result = new System.Collections.Generic.List<Wallet>();
                            results.result.Add(JsonUtility.FromJson<Wallet>(Utilities.JSONArrayWrapper(resultContent)));
                            results.message = "Update wallet complete.";
                            results.httpCode = www.responseCode;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new ListWalletResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while attempting creat a whitelable wallet."
                                : " a HTTP error occured while attempting to creat a whitelable wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Returns the "native" balance for a wallet. This is the balance of the native token used by the chain. Ex. ETH for Ethereum.
        /// </summary>
        /// <remarks>
        /// For more information see <see href="https://docs.venly.io/pages/reference.html#_native_balance_arkane_api">https://docs.venly.io/pages/reference.html#_native_balance_arkane_api</see>
        /// </remarks>
        /// <param name="walletId"></param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator Balance(string walletId, Action<BalanceResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new BalanceResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Wallets.UserWallet.NativeBalance with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new BalanceResult()
                    {
                        hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www =
                        UnityWebRequest.Get(BGSDKSettings.current.WalletUri + "/" + walletId + "/balance");
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new BalanceResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<BalanceResult>(resultContent);
                            results.message = "Wallet balance updated.";
                            results.httpCode = www.responseCode;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new BalanceResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting a user's wallet."
                                : " a HTTP error occured while requesting a user's wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Returns the balance of all tokens currently supported by BGSDK.
        /// </summary>
        /// <remarks>
        /// <para>
        /// For more details see <see href="https://docs.venly.io/pages/reference.html#_token_balances_arkane_api">https://docs.venly.io/pages/reference.html#_token_balances_arkane_api</see>
        /// </para>
        /// </remarks>
        /// <param name="walletId"></param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator TokenBalance(string walletId, Action<TokenBalanceResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new TokenBalanceResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Wallets.UserWallet.TokenBalance with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new TokenBalanceResult()
                    {
                        hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www =
                        UnityWebRequest.Get(BGSDKSettings.current.WalletUri + "/" + walletId + "/balance/tokens");
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new TokenBalanceResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<TokenBalanceResult>(
                                Utilities.JSONArrayWrapper(resultContent));
                            results.message = "Fetch token balance complete.";
                            results.httpCode = www.responseCode;

                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new TokenBalanceResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting the token balance from a wallet."
                                : " a HTTP error occured while requesting the token balance from a wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Returns the token balance for a specified token (this can be any token).
        /// </summary>
        /// <remarks>
        /// <para>
        /// For more details see <see href="https://docs.venly.io/pages/reference.html#_specific_token_balance_arkane_api">https://docs.venly.io/pages/reference.html#_specific_token_balance_arkane_api</see>
        /// </para>
        /// </remarks>
        /// <param name="walletId"></param>
        /// <param name="tokenAddress"></param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator SpecificTokenBalance(string walletId, string tokenAddress,
            Action<TokenBalanceResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new TokenBalanceResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Wallets.UserWallet.TokenBalance with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new TokenBalanceResult()
                    {
                        hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.",
                        result = null
                    });
                    yield return null;
                }
                else
                {
                    UnityWebRequest www = UnityWebRequest.Get(BGSDKSettings.current.WalletUri + "/" + walletId +
                                                              "/balance/tokens/" + tokenAddress);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new TokenBalanceResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results.result = JsonUtility.FromJson<TokenBalance>(resultContent);
                            results.message = "Fetch token balance complete.";
                            results.httpCode = www.responseCode;

                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new TokenBalanceResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting the token balance from a wallet."
                                : " a HTTP error occured while requesting the token balance from a wallet."),
                            result = null, httpCode = www.responseCode
                        });
                    }
                }
            }
        }

        /// <summary>
        /// NFTs can be queried either by wallet ID or by wallet address, if required multiple NFT contract addresses can be passed as a query parameter to act as a filter. 
        /// </summary>
        /// <remarks>
        /// For more information please see <see href="https://docs.venly.io/api/api-products/wallet-api/retrieve-non-fungible-tokens"/>
        /// </remarks>
        /// <param name="walletId"></param>
        /// <param name="optionalContractAddresses">List of contract addresses to filter for, if empty or null all will be returned. Can be null</param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator NFTs(string walletId, SecretType chain, List<string> optionalContractAddresses,
            Action<NFTBalanceResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new NFTBalanceResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Wallets.UserWallet.ListNFTs with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new NFTBalanceResult()
                    {
                        hasError = true,
                        message = "BGSDKSettings.user required, null Settings.user provided."
                    });
                    yield return null;
                }
                else
                {
                    string address = BGSDKSettings.current.WalletUri + "/" + chain.ToString() + "/" + walletId +
                                     "/nonfungibles";

                    if (optionalContractAddresses != null && optionalContractAddresses.Count > 0)
                    {
                        address += "?";
                        for (int i = 0; i < optionalContractAddresses.Count; i++)
                        {
                            if (i == 0)
                                address += "contract-addresses=" + optionalContractAddresses[i];
                            else
                                address += "&contract-addresses=" + optionalContractAddresses[i];
                        }
                    }

                    UnityWebRequest www = UnityWebRequest.Get(address);
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new NFTBalanceResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<NFTBalanceResult>(resultContent);
                            results.message = "List NFTs complete.";
                            results.httpCode = www.responseCode;

                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new NFTBalanceResult()
                        {
                            hasError = true,
                            message = "Error:" + (www.isNetworkError
                                ? " a network error occured while requesting NFTs."
                                : " a HTTP error occured while requesting NFTs."),
                            httpCode = www.responseCode
                        });
                    }
                }
            }
        }
        
        
        
        //--------------------------------------------------------------------------------------------------------------
        
        public enum ImportWalletType
        {
            BITCOIN_PRIVATE_KEY, 
            BSC_PRIVATE_KEY,
            ETHEREUM_PRIVATE_KEY,
            MATIC_PRIVATE_KEY,
            VECHAIN_PRIVATE_KEY,
            HEDERA_PRIVATE_KEY,
            GOCHAIN_PRIVATE_KEY,
            NEO_PRIVATE_KEY,
            AETERNITY_PRIVATE_KEY,
            LITECOIN_PRIVATE_KEY,
            AETERNITY_KEYSTORE,
            BSC_KEYSTORE,
            ETHEREUM_KEYSTORE,
            MATIC_KEYSTORE,
            VECHAIN_KEYSTORE,
            GOCHAIN_KEYSTORE,
            TRON_KEYSTORE,
            NEO_KEYSTORE,
            BITCOIN_WIF,
            LITECOIN_WIF,
            NEO_WIF,
            MIGRATION

        }
        
        [Serializable]
        public class ImportWalletRequest
        {
            public string walletType;
            public string importWalletType;
            public string pincode;
            public string privateKey;
        }
        
        public static IEnumerator ImportWallet(ImportWalletType _importWalletType, int _pincode, string _privatekey, Action<ListWalletResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new ListWalletResult() { hasError = true, message = "Attempted to call BGSDK.Tokens.MintNonFungibleToken with no BGSDK.Settings object applied." });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new ListWalletResult() { hasError = true, message = "BGSDKIdentity required, null identity provided.\nPlease initalize Settings.user before calling Privileged.MintNonFungibleToken" });
                    yield return null;
                }
                else
                {
                    var data = new ImportWalletRequest()
                    {
                        walletType = "WHITE_LABEL",
                        importWalletType = _importWalletType.ToString(),
                        pincode = _pincode.ToString(),
                        privateKey = _privatekey
                    };


                    var request = new UnityWebRequest("https://api-staging.arkane.network/api/wallets/import", "POST");
                    byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data));
                    request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
                    request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
                    request.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
                    request.SetRequestHeader("Content-Type", "application/json");
                    var async = request.SendWebRequest();
                    while (!async.isDone)
                        yield return null;
                    
                    if (!request.isNetworkError && !request.isHttpError)
                    {
                        var results = new ListWalletResult();
                        try
                        {
                            string resultContent = request.downloadHandler.text;
                            results = JsonUtility.FromJson<ListWalletResult>(Utilities.JSONArrayWrapper(resultContent));
                            results.message = "Wallet refresh complete.";
                            results.httpCode = request.responseCode;

                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message = "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = request.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new ListWalletResult() { hasError = true, message = "Error:" + (request.isNetworkError ? " a network error occured while attempting to create offer." : " a HTTP error occured while attempting to mint a token."), httpCode = request.responseCode });
                    }
                }
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

#region Legacy

#if false

        /// <param name="wallet"></param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator Unlink(Wallet wallet, Action<BGSDKBaseResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new BGSDKBaseResult() { hasError = true, message =
 "Attempted to call BGSDK.Wallets.UserWallet.Unlink with no BGSDK.Settings object applied." });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new BGSDKBaseResult() { hasError = true, message =
 "BGSDKSettings.user required, null Settings.user provided." });
                    yield return null;
                }
                else
                {
                    //TODO: Confirm with BGSDK that its the address that should be used. This doesn't appear correct as the value the API expects is a GUID and address is a HEX value
                    UnityWebRequest www =
 UnityWebRequest.Delete(BGSDKSettings.current.WalletUri + "/" + wallet.address + "/link");
                    www.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        callback(new BGSDKBaseResult() { hasError = false, message =
 "Unlink request completed.", httpCode = www.responseCode });
                    }
                    else
                    {
                        callback(new BGSDKBaseResult() { hasError = true, message =
 "Error:" + (www.isNetworkError ? " a network error occured while processing an unlink request." : " a HTTP error occured while requesting the user's wallets."), httpCode
 = www.responseCode });
                    }
                }
            }
        }

        /// <summary>
        /// Returns the list of tokens owned by a wallet grouped by contract
        /// </summary>
        /// <remarks>
        /// <para>
        /// Currently this functionallity is only supported for MATIC wallets and items minted by BGSDK
        /// <para>
        /// For more information please see <see href="https://docs-staging.venly.io/pages/reference.html#_get_inventory_arkane_api">https://docs-staging.venly.io/pages/reference.html#_get_inventory_arkane_api</see>
        /// </para>
        /// </para>
        /// </remarks>
        /// <param name="walletId"></param>
        /// <param name="optionalContractAddresses">List of contract addresses to filter for, if empty or null all will be returned. Can be null</param>
        /// <param name="callback"></param>
        /// <returns>The Unity routine enumerator</returns>
        public static IEnumerator GetInventory(string walletId, List<string> optionalContractAddresses, Action<ListInventoryResults> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new ListInventoryResults() { hasError = true, message =
 "Attempted to call BGSDK.Wallets.UserWallet.ListNFTs with no BGSDK.Settings object applied." });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new ListInventoryResults() { hasError = true, message =
 "BGSDKSettings.user required, null Settings.user provided.", result = null });
                    yield return null;
                }
                else
                {
                    string address = BGSDKSettings.current.WalletUri + "/" + walletId + "/inventory";

                    if (optionalContractAddresses != null && optionalContractAddresses.Count > 0)
                    {
                        address += "?";
                        for (int i = 0; i < optionalContractAddresses.Count; i++)
                        {
                            if (i == 0)
                                address += "contract-addresses=" + optionalContractAddresses[i];
                            else
                                address += "&contract-addresses=" + optionalContractAddresses[i];
                        }
                    }

                    UnityWebRequest www = UnityWebRequest.Get(address);
                    www.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new ListInventoryResults();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results =
 JsonUtility.FromJson<ListInventoryResults>(Utilities.JSONArrayWrapper(resultContent));
                            results.message = "List NFTs complete.";
                            results.httpCode = www.responseCode;

                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
 "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }
                    }
                    else
                    {
                        callback(new ListInventoryResults() { hasError = true, message =
 "Error:" + (www.isNetworkError ? " a network error occured while requesting inventory." : " a HTTP error occured while requesting inventory."), result
 = null, httpCode = www.responseCode });
                    }
                }
            }
        }
#endif

#endregion
    }



        public static class Nft
    {
        public static IEnumerator DeployNftContract(string _name, string _description, string _image,
            string _externalUrlstring, string _chain, string _owner, Action<DeployContractResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new DeployContractResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Tokens.MintNonFungibleToken with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new DeployContractResult()
                    {
                        hasError = true,
                        message =
                            "BGSDKIdentity required, null identity provided.\nPlease initalize Settings.user before calling Privileged.MintNonFungibleToken"
                    });
                    yield return null;
                }
                else
                {
                    var data = new DeployContractRequest()
                    {
                        name = _name,
                        description = _description,
                        image = _image,
                        externalUrl = _externalUrlstring,
                        chain = _chain,
                        owner = _owner
                    };


                    var request =
                        new UnityWebRequest(
                            "https://api-business-staging.arkane.network/api/apps/ec9392e6-2c69-4266-87c3-b9ff4adc377f/contracts",
                            "POST");
                    byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data));
                    request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
                    request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
                    request.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);
                    request.SetRequestHeader("Content-Type", "application/json");
                    var async = request.SendWebRequest();
                    while (!async.isDone)
                        yield return null;


                    if (!request.isNetworkError && !request.isHttpError)
                    {
                        callback(new DeployContractResult()
                        {
                            hasError = false, message = "Successful Deploy Contract", httpCode = request.responseCode
                        });
                    }
                    else
                    {
                        callback(new DeployContractResult()
                        {
                            hasError = true,
                            message = "Error:" + (request.isNetworkError
                                ? " a network error occured while attempting to deploy contract."
                                : " a HTTP error occured while attempting to deploy contract."),
                            httpCode = request.responseCode
                        });
                    }





                }
            }


            yield return null;
        }
    }
        
        
        
        
        
        
        
        
        
        public static class Market
    {
        // #region CREATED OFFER (Need review)
        //
        //
        // public static IEnumerator CreateAnOffer(int _tokenId, string _contractAddress, string _sellerAddress, int _price, Action<BGSDKBaseResult> callback)
        // {
        //     if (BGSDKSettings.current == null)
        //     {
        //         callback(new DataModel.BGSDKBaseResult() { hasError = true, message = "Attempted to call BGSDK.Tokens.MintNonFungibleToken with no BGSDK.Settings object applied." });
        //         yield return null;
        //     }
        //     else
        //     {
        //         if (BGSDKSettings.user == null)
        //         {
        //             callback(new DataModel.BGSDKBaseResult() { hasError = true, message = "BGSDKIdentity required, null identity provided.\nPlease initalize Settings.user before calling Privileged.MintNonFungibleToken" });
        //             yield return null;
        //         }
        //         else
        //         {
        //             var data = new CreateOfferRequest()
        //             {
        //                 type = "SALE",
        //                 nft = new NFT()
        //                 {
        //                     tokenId = _tokenId.ToString(),
        //                     address = _contractAddress.ToString(),
        //                     chain = "MATIC"
        //                 },
        //                 sellerAddress = _sellerAddress.ToString(),
        //                 price = _price
        //             };
        //
        //             
        //             
        //             var request = new UnityWebRequest("https://api-staging.arkane.market/offers", "POST");
        //             byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data));
        //             request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        //             request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        //             request.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
        //             request.SetRequestHeader("Content-Type", "application/json");
        //             var async = request.SendWebRequest();
        //             while (!async.isDone)
        //                 yield return null;
        //             
        //             if (!request.isNetworkError && !request.isHttpError)
        //             {
        //                 callback(new DataModel.BGSDKBaseResult() { hasError = false, message = "Successful create an offer", httpCode = request.responseCode });
        //             }
        //             else
        //             {
        //                 callback(new DataModel.BGSDKBaseResult() { hasError = true, message = "Error:" + (request.isNetworkError ? " a network error occured while attempting to create offer." : " a HTTP error occured while attempting to mint a token."), httpCode = request.responseCode });
        //             }
        //             
        //         }
        //     }
        // }
        //
        // #endregion


    #region OFFER

        public static IEnumerator CreateOffer(string id_nft, string contract_address, string walletAddress,
            string chain, string _price, Action<OfferResult> callback)
        {

            if (BGSDKSettings.current == null)
            {
                //callback(new DefineTokenTypeResult() { hasError = true, message = "Attempted to call BGSDK.Tokens.CreateTokenType with no BGSDK.Settings object applied." });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    //callback(new DefineTokenTypeResult() { hasError = true, message = "BGSDKSettings.user required, null Settings.user provided.\n Please initalize the Settings.user before calling CreateTokenType" });
                    yield return null;
                }
                else
                {
                    DataModel.NFT _nft = new DataModel.NFT()
                    {
                        address = contract_address,
                        tokenId = id_nft,
                        chain = chain
                    };

                    OfferBody body = new OfferBody()
                    {
                        nft = _nft,
                        price = _price,
                        sellerAddress = walletAddress
                    };

                    //now use this def to body
                    var jsonString = JsonUtility.ToJson(body);

                    UnityWebRequest www = UnityWebRequest.Put(BGSDKSettings.current.offerUrl, jsonString);
                    www.method = UnityWebRequest.kHttpVerbPOST;
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);
                    www.uploadHandler.contentType = "application/json;charset=UTF-8";
                    www.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new OfferResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            Debug.Log(resultContent);
                            results = JsonUtility.FromJson<OfferResult>(resultContent);
                            results.message = "Create Offer complete. \n" + resultContent;
                            results.httpCode = www.responseCode;

                            //Info = resultContent;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }

                    }

                }
            }
        }

        public static IEnumerator SignOffer(string walletId, string data, string pincode, string secretType,
            Action<OfferSignatureResult> callback)
        {

            if (BGSDKSettings.current == null)
            {
                Debug.Log("Attempted to call BGSDK.Wallets.List with no BGSDK.Settings object applied.");
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    Debug.Log("BGSDKSettings.user required, null Settings.user provided.");
                    yield return null;
                }
                else
                {
                    OfferSignature signature = new OfferSignature
                    {
                        pincode = pincode,
                        signatureRequest = new SignatureRequest
                        {
                            data = data,
                            walletId = walletId,
                            secretType = secretType

                        }
                    };

                    var jsonString = JsonUtility.ToJson(signature);

                    UnityWebRequest www = UnityWebRequest.Put(
                        BGSDKSettings.current.api[BGSDKSettings.current.useStaging] + "/api/signatures", jsonString);
                    www.method = UnityWebRequest.kHttpVerbPOST;
                    www.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);
                    www.uploadHandler.contentType = "application/json;charset=UTF-8";
                    www.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");

                    var co = www.SendWebRequest();
                    while (!co.isDone)
                        yield return null;

                    if (!www.isNetworkError && !www.isHttpError)
                    {
                        var results = new OfferSignatureResult();
                        try
                        {
                            string resultContent = www.downloadHandler.text;
                            results = JsonUtility.FromJson<OfferSignatureResult>(resultContent);
                            results.message = "Sign Offer complete.";
                            results.httpCode = www.responseCode;
                            Debug.Log(resultContent);
                            //Info = resultContent;
                        }
                        catch (Exception ex)
                        {
                            results = null;
                            results.message =
                                "An error occured while processing JSON results, see exception for more details.";
                            results.exception = ex;
                            results.httpCode = www.responseCode;
                        }
                        finally
                        {
                            callback(results);
                        }

                    }

                }
            }

        }

        public static IEnumerator Sign(string signature, string offerid, UnityAction<string> callback)
        {
            //now use this def to body
            var jsonString = "{\"signature\":\"" + signature + "\" }";

            var www = new UnityWebRequest(BGSDKSettings.current.offerUrl + "/" + offerid + "/signature", "PATCH");
            byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonString);
            www.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            www.uploadHandler.contentType = "application/json";
            www.SetRequestHeader("Authorization",
                BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
            www.SetRequestHeader("Content-Type", "application/json");




            var co = www.SendWebRequest();
            while (!co.isDone)
                yield return null;

            if (!www.isNetworkError && !www.isHttpError)
            {
                var results = new OfferResult();
                try
                {
                    string resultContent = www.downloadHandler.text;
                    results = JsonUtility.FromJson<OfferResult>(resultContent);
                    results.message = "Initiating Offer complete.";
                    results.httpCode = www.responseCode;
                    Debug.Log(resultContent);
                    //Info = resultContent;
                }
                catch (Exception ex)
                {
                    results = null;
                    results.message =
                        "An error occured while processing JSON results, see exception for more details.";
                    results.exception = ex;
                    results.httpCode = www.responseCode;
                }
                finally
                {
                    callback?.Invoke("SUCCESS");
                }

            }
        }

        //SignOffer without wallet ID
        public static IEnumerator SignOfferMetaMask(string walletAddress, string dataToSign, Action<OfferSignatureResult> _callback)
        {
            
            var results = new OfferSignatureResult();
            
            try
            {
                Sign(WalletConnect.ActiveSession,walletAddress,  dataToSign, _callback);
                    
            }
            catch (Exception ex)
            {
                results.message = "An error occured while processing, see exception for more details.";
                results.exception = ex;
            }
            finally
            {
                _callback(results);
            }
        
            yield return null;
        
        }

        async private static void Sign(WalletConnectSession session, string walletAddress, string dataToSign, Action<OfferSignatureResult> callback)
        {
            var results = new OfferSignatureResult();
            string signature = await session.EthSign(walletAddress, dataToSign);
            results.result.signature = signature;
            callback(results);
            Debug.Log(results.result.signature);
        }


#endregion

    #region GET OFFER LIST BY CONTRACT ADDRESS

        public static IEnumerator GetOfferListByContractAddress(Action<ListOfOfferResult> callback)
        {
            // Ethereum TEST FXnRXnTD contract address : 0xeacdf2e92a05dd6896b5f922a17fe62b64c6add0
            // MATIC Apple contract contact address : 0x7102c77fac93571ab61b4aa95424d9a6d86363da
            // MATIC Tangerine contract address : 0xdb616f492df5f668b1b0003d0057b3755659f1b5
            // MATIC TangerineSaleContract address : 0x078a87756611aa51b714799e5c7fd565dd2fe7ff
            UnityWebRequest
                www = UnityWebRequest.Get(
                    "https://api-staging.arkane.market/offers?contractAddress=0xdb616f492df5f668b1b0003d0057b3755659f1b5&status=READY"); //https://api-staging.arkane.market/offers?contractAddress=0x7102c77fac93571ab61b4aa95424d9a6d86363da&status=NEW
            www.SetRequestHeader("Authorization",
                BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);



            var co = www.SendWebRequest();
            while (!co.isDone)
                yield return null;

            if (!www.isNetworkError && !www.isHttpError)
            {
                var results = new ListOfOfferResult();
                try
                {
                    string resultContent = www.downloadHandler.text;
                    results = JsonUtility.FromJson<ListOfOfferResult>(Utilities.JSONArrayWrapper(resultContent));

                }
                catch (Exception ex)
                {
                    results = null;
                }
                finally
                {
                    callback(results);
                }
            }
        }

#endregion

    #region BUY ITEM

        public static IEnumerator BuyItemOffer(string _walletAddress, string _externalUserId, string _offerId,
            Action<BuyItemResult> callback)
        {

            if (BGSDKSettings.current == null)
            {
                callback(new BuyItemResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Tokens.MintNonFungibleToken with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new BuyItemResult()
                    {
                        hasError = true,
                        message =
                            "BGSDKIdentity required, null identity provided.\nPlease initalize Settings.user before calling Privileged.MintNonFungibleToken"
                    });
                    yield return null;
                }
                else
                {
                    var data = new BuyItemRequest()
                    {
                        externalUserId = _externalUserId,
                        walletAddress = _walletAddress
                    };


                    var request = new UnityWebRequest("https://api-staging.arkane.market/offers/" + _offerId + "/buy",
                        "POST");
                    byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data));
                    request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
                    request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
                    request.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);
                    request.SetRequestHeader("Content-Type", "application/json");
                    var async = request.SendWebRequest();
                    while (!async.isDone)
                        yield return null;


                    if (!request.isNetworkError && !request.isHttpError)
                    {
                        callback(new BuyItemResult()
                            {hasError = false, message = "Successful Buy a offer", httpCode = request.responseCode});
                    }
                    else
                    {
                        callback(new BuyItemResult()
                        {
                            hasError = true,
                            message = "Error:" + (request.isNetworkError
                                ? " a network error occured while attempting to buy offer."
                                : " a HTTP error occured while attempting to buy a offer."),
                            httpCode = request.responseCode
                        });
                    }
                }
            }



        }

#endregion



    }


        public static class TokenMint
    {

        private class MintNonFungibleRequest
        {
            public string typeId;
            public string[] destinations;
        }

        private class MintFungibleRequest
        {
            public int[] amounts;
            public string[] destinations;
        }

        public class MintResult : DataModel.BGSDKBaseResult
        {
            public List<ulong> tokenIds;
        }


        public static IEnumerator MintNonFungibleToken(int _tokenId, string _contractId, string[] destinations,
            Action<MintResult> callback)
        {
            if (BGSDKSettings.current == null)
            {
                callback(new MintResult()
                {
                    hasError = true,
                    message =
                        "Attempted to call BGSDK.Tokens.MintNonFungibleToken with no BGSDK.Settings object applied."
                });
                yield return null;
            }
            else
            {
                if (BGSDKSettings.user == null)
                {
                    callback(new MintResult()
                    {
                        hasError = true,
                        message =
                            "BGSDKIdentity required, null identity provided.\nPlease initalize Settings.user before calling Privileged.MintNonFungibleToken"
                    });
                    yield return null;
                }
                else
                {
                    var data = new MintNonFungibleRequest()
                    {
                        typeId = _tokenId.ToString(),
                        destinations = destinations
                    };

                    var request =
                        new UnityWebRequest(
                            "https://api-business-staging.arkane.network/api/apps/ec9392e6-2c69-4266-87c3-b9ff4adc377f/contracts/" +
                            _contractId + "/tokens/non-fungible/", "POST");
                    byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data));
                    request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
                    request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
                    request.SetRequestHeader("Authorization",
                        BGSDKSettings.user.authentication.token_type + " " +
                        BGSDKSettings.user.authentication.access_token);
                    request.SetRequestHeader("Content-Type", "application/json");
                    var async = request.SendWebRequest();

                    while (!async.isDone)
                        yield return null;

                    if (!request.isNetworkError && !request.isHttpError)
                    {
                        //Debug json deserialize - requires triming mint result json
                        string mint_result_json = request.downloadHandler.text.Trim(new char[] {'[', ']'});
                        MintResult mint_result_wrapper = JsonUtility.FromJson<MintResult>(mint_result_json);
                        mint_result_wrapper.httpCode = request.responseCode;
                        mint_result_wrapper.message = "Mint NFT complete";
                        callback(mint_result_wrapper);
                    }
                    else
                    {
                        callback(new MintResult()
                        {
                            hasError = true,
                            message = "Error:" + (request.isNetworkError
                                ? " a network error occured while attempting to mint a token."
                                : " a HTTP error occured while attempting to mint a token."),
                            httpCode = request.responseCode
                        });
                    }
                }
            }
        }


    }





    }
}
#endif