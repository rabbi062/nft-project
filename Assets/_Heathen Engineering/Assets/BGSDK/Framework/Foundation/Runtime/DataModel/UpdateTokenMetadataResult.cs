using System;
using System.Collections.Generic;
using UnityEngine;

namespace HeathenEngineering.BGSDK.DataModel
{
    [Serializable]
    public class UpdateTokenMetadataResult
    {
        
        public string name ;
        public string description ;
        public string image ;
        public string imagePreview ;
        public string imageThumbnail ;
        public string backgroundColor ;
        public string background_color ;
        public string animationUrl ;
        public string animation_url ;
        public string externalUrl ;
        public string external_url ;
        public List<AnimationUrl> animationUrls ;
        public int maxSupply ;
        public List<Attribute> attributes ;
        public Contract contract ;
        public AssetContract asset_contract ;
        public bool fungible ;
        
        
        [Serializable]
        public class AnimationUrl
        {
            public string type ;
            public string value ;
        }
        [Serializable]
        public class Attribute
        {
            public string type ;
            public string name ;
            public string value ;
            public string traitType ;
            public string trait_type ;
            public string maxValue ;
            public string displayType ;
            public string display_type ;
        }
        [Serializable]
        public class Medium
        {
            public string type ;
            public string value ;
        }
        [Serializable]
        public class Contract
        {
            public string address ;
            public string name ;
            public string symbol ;
            public string image ;
            public string imageUrl ;
            public string image_url ;
            public string description ;
            public string externalLink ;
            public string external_link ;
            public string externalUrl ;
            public string external_url ;
            public List<Medium> media ;
            public string type ;
        }
        [Serializable]
        public class AssetContract
        {
            public string address ;
            public string name ;
            public string symbol ;
            public string image ;
            public string imageUrl ;
            public string image_url ;
            public string description ;
            public string externalLink ;
            public string external_link ;
            public string externalUrl ;
            public string external_url ;
            public List<Medium> media ;
            public string type ;
        }
        
        public bool success;
        public bool hasError;
        public string message;
        public long httpCode;
    }
}


