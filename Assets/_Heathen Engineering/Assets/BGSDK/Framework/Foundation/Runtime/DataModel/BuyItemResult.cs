using UnityEngine;
using System;
using HeathenEngineering.BGSDK.DataModel;

[Serializable]
public class BuyItemResult
{
	public string id;
	public string type;
	public string offerId;
	public int amount;
	public string status;
	public DateTime creationDate;
	public int pricePerItem;
	public string buyerId;
	[Serializable]
	public class Buyer
	{
		public string id;
		public string nickname;
	}

	public Buyer buyer;
	public string externalBuyerId;
	public string buyerWalletAddress;
	
	public bool success;
	public bool hasError;
	public string message;
	public long httpCode;
        
}


