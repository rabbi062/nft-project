using UnityEngine;
using System;
using System.Collections.Generic;
using HeathenEngineering.BGSDK.DataModel;

[Serializable]
public class GetWalletByIdentifierResult : BGSDKBaseResult
{
	public List<WalletIdentifier> result;
	[Serializable]
	public class WalletIdentifier
	{
		public string id;
		public string address;
		public string walletType;
		public string secretType;
		public DateTime createdAt;
		public bool archived;
		public string description;
		public bool primary;
		public bool hasCustomPin;
		public string identifier;
		public Balance balance;
		[Serializable]
		public class Balance
		{
			public bool available;
			public string secretType;
			public int balance;
			public int gasBalance;
			public string symbol;
			public string gasSymbol;
			public string rawBalance;
			public string rawGasBalance;
			public int decimals;
		}
	}
        
}


