using UnityEngine;
using System;

[Serializable]
public class CreateOfferRequest
{
	public string type;
	public NFT nft;
	public string sellerAddress;
	public int price;
        
}


