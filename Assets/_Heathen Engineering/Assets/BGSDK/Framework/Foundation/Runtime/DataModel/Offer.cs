using UnityEngine;
using System;
using HeathenEngineering.BGSDK.DataModel;

[System.Serializable]
public class Offer 
{
	public string id;
        [Serializable]
        public class NFT
        {
            [Serializable]
            public class Contract
            {
                public string chain;
                public string address;
                public int count;
                public string name;
                public string description;
                public string symbol;
                public string url;
                public string imageUrl;
            }
    
    
            public string tokenId;
            public string address;
            public string chain;
            public string name;
            public string description;
            public string imageUrl;
            public string url;
            public string imagePreviewUrl;
            public string imageThumbnailUrl;
            public TokenAttributes[] attributes;
            public Contract contract;
        }
    
        public NFT nft;
        public string sellerId;
        public string sellerAddress;
        public DateTime startDate;
        public string type;
        public string status;
        public string dataToSign;
        public string txApprove;
        public string txInCustody;
        public DateTime createdOn;
        public string createdBy;
        public DateTime modifiedOn;
        public string modifiedBy;
        public bool signed;
        public int price;
}


