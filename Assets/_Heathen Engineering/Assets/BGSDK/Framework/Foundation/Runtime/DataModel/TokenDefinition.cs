﻿using System;
using UnityEngine;

namespace HeathenEngineering.BGSDK.DataModel
{

    [Serializable]
    public class TokenDefinition
    {
        /// <summary>
        /// name of the token type
        /// </summary>
        [Tooltip("Name of the token type")]
        public string name;
        /// <summary>
        /// description of the token type
        /// </summary>
        [Tooltip("Description of the token type")]
        public string description;
        [Tooltip("Image url of the token, 2000x2000, preferably svg")]
        public string image;
        public string backgroundColor;
        /// <summary>
        /// The URL with more information about the token
        /// </summary>
        [Tooltip("The URL with more information about the token")]
        public string externalUrl;
        [HideInInspector]
        public ulong currentSupply;
        [HideInInspector]
        public ulong maxSupply;
        public bool fungible;
        public bool burnable;
        
        /// <summary>
        /// Image url of the token, 2000x2000, preferably svg
        /// </summary>
        

        public TypeValuePair[] animationUrls;
        public TokenAttributes[] attributes;

        public virtual string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }

    public class TokenDefinition<T> : TokenDefinition
    {
        public T properties;

        public override string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
