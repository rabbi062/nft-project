using System.Collections.Generic;
using HeathenEngineering.BGSDK.DataModel;
using UnityEngine;

[System.Serializable]
public class ListOfOfferResult
{
	public List<Offer> result;
}


