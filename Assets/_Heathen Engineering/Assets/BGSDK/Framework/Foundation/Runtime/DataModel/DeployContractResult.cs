using System.Collections.Generic;
using HeathenEngineering.BGSDK.DataModel;
using UnityEngine;

namespace HeathenEngineering.BGSDK.DataModel
{
    public class DeployContractResult
    {
	    
	    public string name ;
	    public string description ;
	    public bool confirmed ;
	    public int id ;
	    public string secretType ;
	    public string symbol ;
	    public string externalUrl ;
	    public string image ;
	    public List<Medium> media ;
	    public string transactionHash ;
	    public string owner ;
	    public Storage storage ;
	    public string external_link ;
	    
	    
	    public class Storage
	    {
		    public string type ;
		    public string location ;
	    }
	    
	    public class Medium
	    {
		    public string type ;
		    public string value ;
	    }


	    public bool success;
	    public bool hasError;
	    public string message;
	    public long httpCode;


    }
}


