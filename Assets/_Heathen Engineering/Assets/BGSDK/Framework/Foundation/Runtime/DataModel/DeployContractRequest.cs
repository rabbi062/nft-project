using UnityEngine;
using System;
using System.Collections.Generic;


[Serializable]
public class DeployContractRequest
{
	public string name ;
	public string description ;
	public string image ;
	public string externalUrl ;
	public List<Medium> media ;
	public string chain ;
	public string owner ;
	
	[Serializable]
	public class Medium
	{
		public string type ;
		public string value ;
	}
        
}


