using System;
using System.Collections.Generic;
using UnityEngine;

namespace HeathenEngineering.BGSDK.DataModel
{
	[Serializable]
    public class CreateTokenTypeRequest 
    {
	    public string name ;
	    public string description ;
	    public string image ;
	    public string backgroundColor ;
	    public string externalUrl ;
	    public List<AnimationUrl> animationUrls ;
	    public string maxSupply ;
	    public bool fungible ;
	    public bool burnable ;
	    public List<Attribute> attributes ;
	    [Serializable]
	    public class Attribute
	    {
		    public string type ;
		    public string name ;
		    public string value ;
		    public string maxValue ;
	    }
	    [Serializable]
	    public class AnimationUrl
	    {
		    public string type ;
		    public string value ;
	    }
    }
}


