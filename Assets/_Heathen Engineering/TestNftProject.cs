using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HeathenEngineering.BGSDK.Editor;
using HeathenEngineering.BGSDK.Engine;
using UnityEngine;
using HeathenEngineering.BGSDK.API;
using HeathenEngineering.BGSDK.DataModel;
using HeathenEngineering.BGSDK.Engine;
using UnityEngine.Networking;
using UnityEngine.UI;
using HeathenEngineering.BGSDK.DataModel;


namespace HeathenEngineering.BGSDK.Editor
{
    public class TestNftProject : MonoBehaviour
    {

	    #region Variable
		//--------------------------------------------------------------------------------------------------------------
		[Header("---- Create NFT Type ----")] 
		[SerializeField] private Contract contract;
		[SerializeField] private Token token;

		[Header("---- NFT Mint ----")] 
		[SerializeField] private Token tokenMint;
		[SerializeField] private string[] walletAddress;

		[Header("---- URL Image ----")] 
		[SerializeField] private Image image;
		[SerializeField] private string url;

		public List<Wallet> walletImportData;
	    #endregion

	#region Function

	private void Start()
	{
		
		


	}

	void ImportAWallet()
	{
		// int _pincode = 1114;
		// string _privateKey = "3802121580a8e03e5e5028aaa9343a0351d991db90bfb6ddf1f305e569447e39";
		//
		// this.Wait(5f, () =>
		// {
		// 	StartCoroutine(WalletExtra.ImportWallet(ImportWalletType.MATIC_PRIVATE_KEY ,_pincode, _privateKey, result =>
		// 	{
		// 		if (!result.hasError)
		// 		{
		// 			walletImportData = result.result;
		// 		}
		// 		
		// 	}));
		// });
	}


	void CreateOffer()
	{
		// string _contractAddress = "0x7102c77fac93571ab61b4aa95424d9a6d86363da";
		// int _tokenid = 17; //(Hammer v-654)
		// string _sellerAddress = "0xee4E75d860800772B0b29b0eAF575DA82f7b7905";
		// int _price = 1;
		//
		// this.Wait(5f, () =>
		// {
		// 	StartCoroutine(Market.CreateAnOffer(_tokenid, _contractAddress, _sellerAddress, _price, result =>
		// 	{
		// 		Debug.Log(result.message);
		// 	}));
		// });
	}
	
	

	public void CreateNFTType()
	{
		// StartCoroutine(EditorUtilities.CreateTokenType(contract, token, result =>
		// {
		// 	Debug.Log(result.message);
		// }));
	}
	
	public void MintNFT()
	{
		
		
		// StartCoroutine(Privileged.MintNonFungibleToken(tokenMint, walletAddress, result =>
		// {
		// 	Debug.Log(result.message);
		// }));
	}


	public void StartLoadImage()
	{
		StartCoroutine(GetTextureRequest(url, sprite =>
		{
			image.sprite = sprite;
		}));
	}
	
	
	

	IEnumerator GetTextureRequest(string url, System.Action<Sprite> callback)
	{
		using(var www = UnityWebRequestTexture.GetTexture(url))
		{
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
			}
			else
			{
				if (www.isDone)
				{
					var texture = DownloadHandlerTexture.GetContent(www);
					var rect = new Rect(0, 0, texture.width, texture.height);
					var sprite = Sprite.Create(texture,rect,new Vector2(0f,0f));
					callback(sprite);
				}
			}
		}
	}


	//------------------------------------------------------------------------------------------------------------------
	
	
	private Identity identity;
	private AppId app;
	
	// public IEnumerator DeployContract(string name, string description, Action<DataModel.ContractResult> responce)
	// {
	// 	
 //            
 //
 //            if (string.IsNullOrEmpty(name))
 //            {
 //                responce(new DataModel.ContractResult() { hasError = true, message = "name required, null or empty name provided.", result = null });
 //                yield return null;
 //            }
 //            else if (identity == null)
 //            {
 //                responce(new DataModel.ContractResult() { hasError = true, message = "BGSDKIdentity required, null identity provided.", result = null });
 //                yield return null;
 //            }
 //            else
 //            {
 //                WWWForm form = new WWWForm();
 //                form.AddField("name", name);
 //                form.AddField("description", description);
 //
 //                UnityWebRequest www = UnityWebRequest.Post(BGSDKSettings.current.GetContractUri(app), form);
 //
 //                www.SetRequestHeader("Authorization", identity.authentication.token_type + " " + identity.authentication.access_token);
 //
 //                var co = www.SendWebRequest();
 //                if (!co.isDone)
 //                     yield return null;
 //
 //                if (!www.isNetworkError && !www.isHttpError)
 //                {
 //                    string resultContent = www.downloadHandler.text;
 //                    var results = new DataModel.ContractResult();
 //                    results.result = JsonUtility.FromJson<DataModel.ContractData>(Utilities.JSONArrayWrapper(resultContent));
 //                    results.message = "Deploy Contract complete.";
 //                    results.httpCode = www.responseCode;
 //                    responce(results);
 //                    
 //                }
 //                else
 //                {
 //                    responce(new DataModel.ContractResult() { hasError = true, message = "Error:" + (www.isNetworkError ? " a network error occured while attempting to deploy a contract." : " a HTTP error occured while attempting to deploy a contracts."), result = null, httpCode = www.responseCode });
 //                }
 //            }
 //            
 //            
 //        }
	//
	// //------------------------------------------------------------------------------------------------------------------
	// public IEnumerator CreateTokenType(Contract contract, Token token)
 //        {
	//         
	//         
	//         var request = new UnityWebRequest(BGSDKSettings.current.DefineTokenTypeUri(contract), "POST");
	//         byte[] bodyRaw = Encoding.UTF8.GetBytes(token.CreateTokenDefitionJson());
	//         request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
	//         request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
	//         request.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
	//         request.SetRequestHeader("Content-Type", "application/json");
	//         yield return request.SendWebRequest();
 //            //Define a type of token
 //            //yield return null;
 //
 //            // if (string.IsNullOrEmpty(BGSDKSettings.current.appId.clientSecret) || string.IsNullOrEmpty(BGSDKSettings.current.appId.clientId))
 //            // {
 //            //     Debug.LogError("Failed to sync settings: you must populate the Client ID and Client Secret before you can sync settings.");
 //            //     yield return null;
 //            // }
 //            //
 //            // else
 //            // {
 //            //     
 //            //
 //            //     // if (!request.isNetworkError && !request.isHttpError)
 //            //     // {
 //            //     //     string resultContent = request.downloadHandler.text;
 //            //     //     var results = new DataModel.CreateTokenTypeResult();
 //            //     //     results.result = JsonUtility.FromJson<TokenCreateResponceData>(resultContent);
 //            //     //     results.message = "Define Token Type complete.";
 //            //     //     results.httpCode = request.responseCode;
 //            //     //     responce(results);
 //            //     // }
 //            //     // else
 //            //     // {
 //            //     //     responce(new DataModel.CreateTokenTypeResult() { hasError = true, message = "Error:" + (request.isNetworkError ? " a network error occured while attempting to define the token type." : " a HTTP error occured while attempting to define the token type."), httpCode = request.responseCode });
 //            //     // }
 //            // }
 //        }
 //
 //
	// IEnumerator CreateTokenForContract(Contract contract, Token token)
	// {
	// 	var settings = BGSDKSettings.current;
	// 	
	// 	var authenticated = false;
 //
	// 	WWWForm authForm = new WWWForm();
	// 	authForm.AddField("grant_type", "client_credentials");
	// 	authForm.AddField("client_id", BGSDKSettings.current.appId.clientId.Trim());
	// 	authForm.AddField("client_secret", BGSDKSettings.current.appId.clientSecret.Trim());
 //
	// 	UnityWebRequest auth_www = UnityWebRequest.Post(BGSDKSettings.current.AuthenticationUri, authForm);
 //
	// 	var ao = auth_www.SendWebRequest();
 //
	// 	while (!ao.isDone)
	// 	{
	// 		yield return null;
	// 	}
 //
	// 	if (!auth_www.isNetworkError && !auth_www.isHttpError)
	// 	{
	// 		string resultContent = auth_www.downloadHandler.text;
	// 		if (BGSDKSettings.user == null)
	// 			BGSDKSettings.user = new Identity();
 //
	// 		var auth = JsonUtility.FromJson<AuthenticationResponce>(resultContent);
	// 		BGSDKSettings.user.authentication = auth;
	// 		BGSDKSettings.user.authentication.not_before_policy = resultContent.Contains("not-before-policy:1");
	// 		BGSDKSettings.user.authentication.Create();
	// 		authenticated = true;
	// 	}
	// 	else
	// 	{
	// 		Debug.LogError((auth_www.isNetworkError ? "Error on authentication: Network Error." : "Error on authentication: HTTP Error.") + "\n" + auth_www.error);
	// 	}
 //
 //
	// 	if (authenticated)
	// 	{
	// 		// Contract auth
	// 		contract.tokens.Add(token);
	// 		
	// 		var newTokens = contract.tokens.Where(p => !p.UpdatedFromServer);
	// 		foreach (var t in newTokens)
	// 		{
	// 			yield return null;
	// 		
	// 			if (string.IsNullOrEmpty(BGSDKSettings.current.appId.clientSecret) || string.IsNullOrEmpty(BGSDKSettings.current.appId.clientId))
	// 			{
	// 				Debug.LogError("Failed to sync settings: you must populate the Client ID and Client Secret before you can sync settings.");
	// 				yield return null;
	// 			}
	// 			else if (string.IsNullOrEmpty(t.SystemName))
	// 			{
	// 				Debug.LogError("Failed to create token [" + t.SystemName + "] for contract [" + contract.SystemName + "], message: name required, null or empty name provided.");
	// 				yield return null;
	// 			}
	// 			else
	// 			{
	// 				var request = new UnityWebRequest(BGSDKSettings.current.DefineTokenTypeUri(contract), "POST");
	// 				Debug.Log(BGSDKSettings.current.DefineTokenTypeUri(contract));
	// 				byte[] bodyRaw = Encoding.UTF8.GetBytes(t.CreateTokenDefitionJson());
	// 			
	// 				request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
	// 				request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
	// 				request.SetRequestHeader("Authorization", BGSDKSettings.user.authentication.token_type + " " + BGSDKSettings.user.authentication.access_token);
	// 				request.SetRequestHeader("Content-Type", "application/json");
	// 				yield return request.SendWebRequest();
 //
	// 				if (!request.isNetworkError && !request.isHttpError)
	// 				{
	// 					Debug.Log("Created token [" + t.SystemName + "] for contract [" + contract.SystemName + "]");
	// 				}
	// 				else
	// 				{
	// 					Debug.LogError("Failed to create token [" + t.SystemName + "] for contract [" + contract.SystemName + "], error:  " + request.responseCode + " message: " + "Error:" + (request.isNetworkError ? " a network error occured while attempting to define the token type." : " a HTTP error occured while attempting to define the token type."));
	// 				}
	// 			}
	// 		}
 //
	// 		
 //
	// 	}
	// 	
	// 	
	// 	
	// 	
	// }
	
	
	
	#endregion

    }
}


