﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.DataModel;
using HeathenEngineering.BGSDK.Engine;
using TD;
using TMPro;
//using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HeathenEngineering.BGSDK.Examples
{
    [Serializable]
    public class AllNftName
    {
        public List<string> name;
        

    }

    [Serializable]
    public class ContractDataForJson
    {
        public string contractName;
        public string contractAddress;
    }
    
    
    public class ApiExamples : MonoBehaviour
    {

        #region Singleton

        public static ApiExamples singleton;

        private void Awake()
        {
            if (singleton == null)
            {
                singleton = this;
            }
        }

        #endregion



        public Button nextScene;
        
        
        [Header("---- Token ----")]
        public string tokenPath;

        public List<ContractDataForJson> contractJson = new List<ContractDataForJson>();

        [Header("---- Settings ----")] 
        [SerializeField] private Button import_Button;
        [SerializeField] private GameObject loading;

        [Space(20)]
        public InputField walletId;
        public InputField walletPin;
        public TMP_InputField walletAddress;
        public Dropdown createWalletType;
        public TMP_Dropdown listNFTWalletType;
        private SecretType chainType = SecretType.MATIC;

        [Header("Results")]
        public List<Wallet> walletData;
        public List<NFTBalanceResult.Token> tokenData;

        private List<string> allNftNames = new List<string>();
        
        private List<string> tempNamelist = new List<string>();
        private string tId;
        private string _contrctAddress;
        private string _walletId;
        private string _chain;
        //--------------------------------------------------------------------------------------------------------------

        private void Start()
        {
           // Debug.Log(Application.persistentDataPath);
           
           nextScene.onClick.RemoveAllListeners();
           nextScene.onClick.AddListener(() =>
           {
               if (LevelManager.singleton != null)
               {
                   LevelManager.singleton.LoadLevel("HomeScene");
               }
           });
            
            
            import_Button.interactable = false;
            loading.SetActive(false);
            import_Button.gameObject.SetActive(true);
            
            
            string wAddress = PlayerPrefs.GetString(GlobalName.currentUserWalletAddress);
            if (string.IsNullOrEmpty(wAddress))
            {
                walletAddress.gameObject.SetActive(true);
            }
            else
            {
                walletAddress.gameObject.SetActive(false);
            }
            
            import_Button.onClick.RemoveAllListeners();
            import_Button.onClick.AddListener(() =>
            {
                FetchNFTs();
                
                loading.SetActive(true);
                import_Button.gameObject.SetActive(false);
                
                
                
                
            });
            
        }

        #region CREATE WALLET

        public void CreateWallet()
        {
            if (!string.IsNullOrEmpty(walletId.text)
                && !string.IsNullOrEmpty(walletPin.text))
            {
                StartCoroutine(API.Server.Wallets.Create(walletPin.text, walletId.text, (SecretType)createWalletType.value, HandleCreateWalletResult));
            }
            else
            {
                Debug.LogWarning("You must provide a wallet ID and Pin in order to create a new wallet.");
            }
        }
        
        
        private void HandleCreateWalletResult(ListWalletResult walletResult)
        {
            if (!walletResult.hasError)
            {
                walletData = walletResult.result;
                Debug.Log("Create Wallets Responce:\nReturned " + walletResult.result.Count.ToString() + " wallets. You can review the results in the inspector.");
            }
            else
            {
                Debug.Log("Create Wallets Responce:\nHas Error: " + walletResult.hasError + "\nMessage: " + walletResult.message + "\nCode:" + walletResult.httpCode);
            }
        }

        #endregion

        #region FETCH WALLET DATA + FETCH NFT DATA
        
        
        private void HandleListWalletResults(ListWalletResult walletResult)
        {
            if (!walletResult.hasError)
            {
                walletData = walletResult.result;
                StartCoroutine(FetchWalletId());

            }
            else
            {
                Debug.Log("List Wallets Responce:\nHas Error: " + walletResult.hasError + "\nMessage: " + walletResult.message + "\nCode:" + walletResult.httpCode);
            }
        }
        

        IEnumerator FetchWalletId()
        {
            
            foreach (var walletD in walletData)
            {
                if (walletD.address == PlayerPrefs.GetString(GlobalName.currentUserWalletAddress))
                {
                    _walletId = walletD.id;
                    PlayerPrefs.SetString(GlobalName.currentUserWalletId, _walletId);
                    _chain = walletD.secretType;
                    PlayerPrefs.SetString(GlobalName.currentUserWalletChain, _chain);
                }
            }
            yield return null;
            
            StartCoroutine(API.Server.Wallets.NFTs(PlayerPrefs.GetString(GlobalName.currentUserWalletAddress), (SecretType)listNFTWalletType.value, null, HandleNFTBalanceResult));
        }
        
        
        public void FetchNFTs()
        {
            // Get Wallet Address
            string wAddress = PlayerPrefs.GetString(GlobalName.currentUserWalletAddress);
            if (string.IsNullOrEmpty(wAddress))
            {
                wAddress = walletAddress.text;
                PlayerPrefs.SetString(GlobalName.currentUserWalletAddress, wAddress);
            }
            
            StartCoroutine(API.Server.Wallets.List(HandleListWalletResults));
            
            
            
        }

        private void HandleNFTBalanceResult(NFTBalanceResult balanceResult)
        {
            if(!balanceResult.hasError)
            {
                tokenData = balanceResult.result;

                // var t = Resources.LoadAll("Token/", typeof(Token));
                //
                // foreach (var tkn in t)
                // {
                //     foreach (var to in tokenData)
                //     {
                //         if (tkn.name == to.name)
                //         {
                //             return;
                //         }
                //     }
                // }
                
                
                StartCoroutine(ProcessNFTData());

            }
            else
            {
                Debug.Log("List NFT Responce:\nHas Error: " + balanceResult.hasError + "\nMessage: " + balanceResult.message + "\nCode:" + balanceResult.httpCode);
            }
        }


        public IEnumerator ProcessNFTData()
        {
            

            for (int i = 0; i < contractJson.Count; i++)
            {
                if (contractJson[i].contractName == "Tangerine Contract")
                {
                    _contrctAddress = contractJson[i].contractAddress;
                }
            }
            tempNamelist.Clear();
            StartCoroutine(AddHeroDataListToServer.FetchHeroListData(delegate(List<AllHeroNftName> result)
                    {
                        if (result.Count > 0)
                        {
                            for (int i = 0; i < result.Count; i++)
                            {
                                try
                                {
                                    tempNamelist.Add(result[i].name.ToString());
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                    throw;
                                }
                            }

                            for (int i = 0; i < tokenData.Count; i++)
                            {
                
                
                //
                                var arkaneToken = ScriptableObject.CreateInstance<Token>();
                                arkaneToken.SystemName = tokenData[i].name;
                                arkaneToken.Description = tokenData[i].description;
                                arkaneToken.Image = tokenData[i].imageUrl;
                //Attribute data
                
                                var h = arkaneToken.GetTokenDefinition();
                                h.attributes = new TokenAttributes[tokenData[i].attributes.Length];
                                for (int j = 0; j < tokenData[i].attributes.Length; j++)
                {
                    h.attributes[j].name = tokenData[i].attributes[j].name;
                    h.attributes[j].type = tokenData[i].attributes[j].type;
                    h.attributes[j].value = tokenData[i].attributes[j].value;
                    h.attributes[j].maxValue = tokenData[i].attributes[j].maxValue;
                }

                                h.maxSupply = tokenData[i].maxSupply;
                
                

                                tId = tokenData[i].id;
                
                                arkaneToken.Id = tId;
                                arkaneToken.Address = _contrctAddress;
                                string name = tokenData[i].name + tokenData[i].description;

                
                
                
                                foreach (var v in tokenData[i].attributes)
                                {

                                    switch (v.name)
                                    {
                                        case "Attack Damage":
                                            
                                            break;
                                        case "Farm Power":
                                            
                                            break;
                                        case "Type":
                                            if (v.value == "Hero")
                                            {
                                                if (tempNamelist.Contains(name))
                                                {
                                    
                                                }
                                                else
                                                {
                                                    StartCoroutine(AddHeroDataListToServer.UpdateAllHeroNameList(name));
                                                }

                                            }
                                            
                                            break;
                                    }
                    
                                }

                
                                // if (PlayerUpdateTokenData.singleton != null)
                // {
                //     string _fileName = arkaneToken.SystemName + arkaneToken.Description + "_" + arkaneToken.Id;
                //     
                //     if (!PlayerUpdateTokenData.singleton.IsUpdateTokenFileExist(_fileName))
                //     {
                //         StartCoroutine(PlayerUpdateTokenData.singleton.SaveUpdatedToken(arkaneToken, s =>
                //         {
                //             Debug.Log(s);
                //         }));
                //         
                //     }
                //         
                // } 
                
                                StartCoroutine(SaveGameSo(arkaneToken, name, arg0 =>
                                    {
                    
                                    }));
                
                
                
                
                            }
                            
                            
                        }
					
					
                    })
                );
            
            

                
            
            yield return null;


            for (int i = 0; i < tokenData.Count; i++)
            {
                string name = tokenData[i].name + tokenData[i].description;


                foreach (var v in tokenData[i].attributes)
                {

                    switch (v.name)
                    {
                        case "Attack Damage":
                            allNftNames.Add(name);
                            break;
                        case "Farm Power":
                            allNftNames.Add(name);
                            break;
                        case "Type":

                            break;
                    }

                }


            }

            var data = new AllNftName
            {
                name = allNftNames
            };
            
            string saveData = JsonUtility.ToJson(data, true);
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath  + GlobalName.tokenPath + GlobalName.allNFTFileName);
            bf.Serialize(file, saveData);
            file.Close();
            
            this.Wait(10f, () =>
            {
                if (LevelManager.singleton != null)
                {
                    LevelManager.singleton.LoadLevel("HomeScene");
                }
            });

        }
        

        #endregion

        #region SCRIPTABLE DATA SAVE

        public bool IsSaveFile()
        {
            return Directory.Exists(Application.persistentDataPath + "/Data");
        }
        
        
        
        //public void SaveGameSO(Token token)
        public IEnumerator SaveGameSo(Token token, string filename , UnityAction<string> result)
        {
            if (!IsSaveFile())
            {
                Directory.CreateDirectory(Application.persistentDataPath  + "/Data");
            }
            
            // Token
            if (!Directory.Exists(Application.persistentDataPath  + GlobalName.tokenPath))
            {
                Directory.CreateDirectory(Application.persistentDataPath  + GlobalName.tokenPath);
            }

            string saveData = JsonUtility.ToJson(token, true);
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath  + GlobalName.tokenPath + filename);
            bf.Serialize(file, saveData);
            file.Close();

            yield return null;
            
            result?.Invoke("SUCCESS");
        }
        
        
        
        
        public string RootPath
        {
            get{
                if(Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android) 
                {
                    string tempPath = Application.persistentDataPath, dataPath;
                    if (!string.IsNullOrEmpty (tempPath)) {
					
                        dataPath = PlayerPrefs.GetString ("DataPath", "");
                        if (string.IsNullOrEmpty (dataPath)) {
                            PlayerPrefs.SetString ("DataPath", tempPath);
                        }
					
                        return tempPath + "/";
                    } else {
                        Debug.Log ("Application.persistentDataPath Is Null.");
					
                        dataPath = PlayerPrefs.GetString ("DataPath", "");
					
                        return dataPath + "/";
                    }
                }
                else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
                {
                    return Application.dataPath.Replace ("Assets", "");
                }
                else
                {
                    return Application.dataPath + "/";
                }
            }
        }

        #endregion
        
        
        
        
        
        
    }
}
