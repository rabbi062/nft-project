using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace TD
{
    public class PlayfabServiceManager : MonoBehaviour
    {
	#region Variable


	public NftExclusivityResult nftExclusivityData;


	private const string NFT_EXCLUSIVITY_NAME = "NFT Exclusivity";

	#endregion

	#region Function

	private void Start()
	{
		if (PlayFabAuthService.PlayfabSuccessfulLogin)
		{
			
		}
	}


	public void GetTitleData()
	{
		
		
		PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(), 
			result =>
		{
			if (result.Data == null || !result.Data.ContainsKey(NFT_EXCLUSIVITY_NAME)) Debug.Log(NFT_EXCLUSIVITY_NAME);
			else
			{
				Debug.Log("NftName: " + result.Data[NFT_EXCLUSIVITY_NAME]);

				nftExclusivityData = JsonUtility.FromJson<NftExclusivityResult>(result.Data[NFT_EXCLUSIVITY_NAME]);

			}

			

		}, error =>
		{
			Debug.Log("Got error getting titleData:");
			Debug.Log(error.GenerateErrorReport());
		});
	}
	
	
	
	public void UpdateUserInternalData() 
	{
		PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
		{
			Data = new Dictionary<string, string>() 
			{
				{"C", "Fighter"},
				{"R", "Human"},
				
			}
		}, result =>
		{
			Debug.Log("Set internal user data successful");
		}, error =>
		{
			Debug.Log("Got error updating internal user data:");
			Debug.Log(error.GenerateErrorReport());
		});
	}
	

	#endregion
        
    }
    
    
    [Serializable]
    public class NftExclusivityResult
    {
	    public List<NftExclusivity> result;
		[Serializable]
	    public class NftExclusivity
	    {
		    public string id;
		    public string name;
	    }
	    
    }
}


