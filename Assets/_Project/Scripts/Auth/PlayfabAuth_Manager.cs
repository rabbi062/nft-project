using System;
using System.Collections;
using System.Text.RegularExpressions;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace TD
{
    public class PlayfabAuth_Manager : UIBaseSC
    {
		#region Variable

		[SerializeField] private bool clearPlayerLogin;
		
		[Header("---- Button ----")] 
		[SerializeField] private Button emailLoginButton;

		
		
		[SerializeField] private Button loginButton;
		[SerializeField] private Button registerButton;
		



		[Header("---- Login ----")] 
		[SerializeField] private TMP_InputField loginEmail;
		[SerializeField] private TMP_InputField loginPassword;
		[SerializeField] private Toggle rememberMe;
		
		[Header("---- Register ----")] 
		[SerializeField] private TMP_InputField registerEmail;
		[SerializeField] private TMP_InputField registerPassword;
		[SerializeField] private TMP_InputField username;
		



		private PlayFabAuthService _AuthService = PlayFabAuthService.Instance;
		private const string emailPattern = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";


		private const string loginPanel = "LOGIN";
		private const string registerPanel = "REGISTER";
		private const string loadingPanel = "LOADING";
		private const string errorNetworkPanel = "NETWORK ERROR";
		private const string buttonGroup = "BUTTON GROUP";
		
		
		#endregion

		#region Function

		private void Start()
		{
			MDisableAllScreens();
			ShowMenu(loadingPanel);
			
			// Check Internet
			StartCoroutine(CheckInternet(str =>
			{
				if (str == "SUCCESS")
				{
					if (_AuthService.RememberMe)
					{
						CloseMenu(buttonGroup);
						//Start the authentication process.(Automatic login)
						_AuthService.Authenticate();
					}
					else
					{
						ShowMenu(buttonGroup, false);
					}
				}
				
			}));

			
			
			

			//Reset
			if (clearPlayerLogin)
			{
				_AuthService.UnlinkSilentAuth();
				_AuthService.ClearRememberMe();
				_AuthService.AuthType = Authtypes.None;
			}
			
			// Remember me
			rememberMe.isOn = _AuthService.RememberMe;
			rememberMe.onValueChanged.AddListener((toggle) =>
			{
				_AuthService.RememberMe = toggle;
			});
			
			
			
			
			
			loginButton.onClick.AddListener(OnLoginClicked);
			registerButton.onClick.AddListener(OnRegisterClicked);
			
			
			//
			PlayFabAuthService.OnLoginSuccess += OnLoginSuccess;
			PlayFabAuthService.OnPlayFabError += OnPlayFaberror;
			
			
			
			// 
			emailLoginButton.onClick.RemoveAllListeners();
			emailLoginButton.onClick.AddListener(() =>
			{
				ShowMenu(loginPanel);
			});
		}


		private void OnLoginClicked()
		{
			if (loginEmail.text.Contains("@"))
			{
				if (ValidateEmail(loginEmail.text) && ValidatePassword(loginPassword.text))
				{
					_AuthService.Email = loginEmail.text;
					_AuthService.Password = loginPassword.text;
					_AuthService.Authenticate(Authtypes.EmailAndPassword);
				}
			}
		}

		private void OnRegisterClicked()
		{
			if (registerEmail.text.Contains("@"))
			{
				if (ValidateEmail(registerEmail.text) && ValidatePassword(registerPassword.text))
				{
					_AuthService.Email = registerEmail.text;
					_AuthService.Username = username.text;
					_AuthService.Password = registerPassword.text;
					_AuthService.Authenticate(Authtypes.RegisterPlayFabAccount);
				}
			}
		}


		#region Playfab Callback
		
		private void OnPlayFaberror(PlayFabError error)
		{
			//Error List
			switch (error.Error)
			{
				case PlayFabErrorCode.InvalidEmailAddress:
				case PlayFabErrorCode.InvalidPassword:
				case PlayFabErrorCode.InvalidEmailOrPassword:
					Debug.Log("Invalid Email or Password");
					break;

				case PlayFabErrorCode.AccountNotFound:
					 ShowMenu(registerPanel);
					return;
				case PlayFabErrorCode.DuplicateUsername:
					Debug.Log("Duplicate UserName");
					break;
				default:
					Debug.Log(error.GenerateErrorReport());
					break;
                
			}
			
			
			Debug.Log(error.Error);
			Debug.LogError(error.GenerateErrorReport());
		}

		private void OnLoginSuccess(PlayFab.ClientModels.LoginResult result)
		{
			MDisableAllScreens();
			
			var request = new GetAccountInfoRequest();
			PlayFabClientAPI.GetAccountInfo(request, result =>
				{
					
					PlayerPrefsUtility.SetPlayerPrefsString(GlobalName.PLAYFAB_ID, result.AccountInfo.PlayFabId);
					PlayerPrefs.SetString(GlobalName.playerUsername, result.AccountInfo.Username);
					
				},
				error =>
				{
					Debug.LogError(error.GenerateErrorReport());
				});


			if (LevelManager.singleton != null)
			{
				LevelManager.singleton.LoadLevel("ShopScene");
			}
		}
		
		
		
		#endregion
		
		
		#region Helper Function

		IEnumerator CheckInternet(UnityAction<string> callback)
		{
			yield return new WaitForSeconds(2f);
			
			UnityWebRequest request = new UnityWebRequest("http://google.com");
			yield return request.SendWebRequest();

			if (request.error != null)
			{
				MDisableAllScreens();
				ShowMenu(errorNetworkPanel);
				
				callback?.Invoke("ERROR");

				this.Wait(3f, () =>
				{
					Application.Quit();
					
				});
			}
			else
			{
				MDisableAllScreens();
				callback?.Invoke("SUCCESS");
			}
			
		}
		
		/// Validates the email.
		public bool ValidateEmail(string em)
		{
			return Regex.IsMatch(em, emailPattern);
		}
		
		/// Validates the password.

		public bool ValidatePassword(string p1)
		{
			return (p1.Length > 5);
		}

		#endregion
		
		#endregion
        
    }
}


