using UnityEngine;

namespace TD
{
	[CreateAssetMenu(fileName = "so_HeroItem", menuName = "Scriptable Objects/Inventory/HeroItem")]
    public class SO_Hero : SO_InventoryItemBase
    {
	    
	    [Space(10)] 
	    public int heroLevel;
	    public GameObject heroPrefab_UI;

	    public int heroHp;


    }
}


