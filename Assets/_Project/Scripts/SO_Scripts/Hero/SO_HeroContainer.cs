using System.Collections.Generic;
using UnityEngine;

namespace TD
{
	[CreateAssetMenu(fileName = "Hero Container", menuName = "Scriptable Objects/Hero/Container")]
    public class SO_HeroContainer : ScriptableObject
    {
	#region Variable
	public List<HeroSlot> HeroContainer = new List<HeroSlot>();
	
	#endregion

	#region Function
	
	public void AddItemHero(SO_Hero _item)
	{
		bool hasItem = false;
		for (int i = 0; i < HeroContainer.Count; i++)
		{
			if (HeroContainer[i].item == _item)
			{
				hasItem = true;
				break;
			}
		}

		if (!hasItem)
		{
			HeroContainer.Add(new HeroSlot(_item));
		}
		
	}
	
	#endregion
        
    }
    
    [System.Serializable]
    public class HeroSlot
    {
	    public SO_Hero item;

	    public HeroSlot(SO_Hero _item)
	    {
		    item = _item;
	    }

    }
}


