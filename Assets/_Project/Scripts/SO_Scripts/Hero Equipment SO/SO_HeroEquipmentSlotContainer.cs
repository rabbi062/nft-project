using System.Collections.Generic;
using UnityEngine;

namespace TD
{
	[CreateAssetMenu(fileName = "Hero Equipment Container", menuName = "Scriptable Objects/Inventory/Container")]
    public class SO_HeroEquipmentSlotContainer : ScriptableObject
    {
	#region Variable
	
	public List<InventorySlot> Container = new List<InventorySlot>();
	
	
	#endregion

	#region Function
	
	
	
	public void AddItemWeapon(SO_WeaponItem _item)
	{
		bool hasItem = false;
		for (int i = 0; i < Container.Count; i++)
		{
			if (Container[i].item == _item)
			{
				hasItem = true;
				break;
			}
		}

		if (!hasItem)
		{
			Container.Add(new InventorySlot(_item));
		}
		
	}
	
	#endregion
        
    }
    
    
    [System.Serializable]
    public class InventorySlot
    {
	    public SO_WeaponItem item;

	    public InventorySlot(SO_WeaponItem _item)
	    {
		    item = _item;
	    }

    }
}


