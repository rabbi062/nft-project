using UnityEngine;

namespace TD
{
	[CreateAssetMenu(fileName = "so_WeaponItem", menuName = "Scriptable Objects/Inventory/WeaponItem")]
    public class SO_WeaponItem : SO_InventoryItemBase
    {
	    [Space(10)] 
	    public int weaponLevel;
	    public GameObject weaponPrefab_UI;
	    
	    public int weaponAttack;
	    public GameObject attackPrefab_UI;
	    
	    public int weaponDefense;
	    public GameObject defensePrefab_UI;

	    public int weaponFarm;
	    public GameObject farmPrefab_UI;

    }
}


