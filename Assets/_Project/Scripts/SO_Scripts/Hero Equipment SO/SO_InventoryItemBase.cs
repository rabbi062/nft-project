using UnityEngine;

namespace TD
{
    public class SO_InventoryItemBase : ScriptableObject
    {
        public int ID;
        public string tokenId;
        public InventoryItemType inventoryItemType;
        public string name;
        public GameObject itemPrefab;
        public GameObject slotPrefabUI;
        public Sprite itemIcon;
        [TextArea(5, 10)] public string description;
    }


    public enum InventoryItemType
    {
        ActionWeapon,
        Farmweapon,
        Shield,
        Hero
    }
}


