using UnityEngine;

namespace TD
{
	[CreateAssetMenu(fileName = "so_playerStat", menuName = "Scriptable Objects/Player/Stat")]
    public class SOPlayerStat : ScriptableObject
    {
		[Header("---- Controller ----")]
	    public float playerMoveSpeed;
	    public float jumpSpeed;
		
	    [Header("---- Equip Object ----")]
	    public Sprite farmWeapon_Icon;
	    public GameObject farmWeapon_Object;
	    public string farmTokenId;
	    public string farmJsonUpdateFileName;
	    public Sprite actionWeapon_Icon;
	    public GameObject actionWeapon_Object;
	    public string actionTokenId;
	    public string actionJsonUpdateFileName;

	    [Header("---- Stat ----")] 
	    public int hp;
	    public int attack;
	    public int defense;
	    public int farmPower;

    }
}


