using System;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.Engine;
using HeathenEngineering.BGSDK.Examples;
using UnityEngine;
using UnityEngine.Networking;

namespace TD
{
    public class SO_DATAController : MonoBehaviour
    { 
	    //--------------------------------------------------------------------------------------------------------------

	    #region Singleton (DONTDESTROY)
	    public static SO_DATAController singleton;
	    
	    void Awake()
	    {
		    OnAwake();
		    if (singleton == null)
		    {
			    singleton = this;
		    }
		    else
		    {
			    Destroy(this.gameObject);
		    }
		    DontDestroyOnLoad(this.gameObject);

		    //DataGet();
	    }

	    #endregion
	    
		#region Variable


		
		[HideInInspector] public SO_HeroEquipmentSlotContainer equipmentContainer;
		public Dictionary<int, SO_WeaponItem> tokenFetched = new Dictionary<int, SO_WeaponItem>();
		[HideInInspector] public SOPlayerStat playerStatSO = null;
		
		private int index;
	
		#endregion

		#region Function
	
		//--------------------------------------------------------------------------------------------------------------
		void OnAwake()
		{
			
			
			
			
		}

		private void Start()
		{
			index = 0;
			if (playerStatSO == null)
			{
				playerStatSO = Resources.Load<SOPlayerStat>("Player/so_playerStat");
			}
			
			

			UpdatePlayerEquipedItem();
			DataSetHeroEquipmentContainer();
			
			
			
		}

		//--------------------------------------------------------------------------------------------------------------
		public void DataSetHeroEquipmentContainer()
		{
			if (equipmentContainer == null)
			{
				equipmentContainer = Resources.Load<SO_HeroEquipmentSlotContainer>("Hero Equipment Container");
			}
			equipmentContainer.Container.Clear();
		}


		public void UpdatePlayerEquipedItem()
		{
			PlayerUpdateTokenData.singleton.RemoveEquipedItemToContainer();
			
			if (playerStatSO.actionJsonUpdateFileName != null)
			{
				PlayerUpdateTokenData.singleton.AddEquipedItemToContainer(playerStatSO.actionJsonUpdateFileName);
			}

			if (playerStatSO.farmJsonUpdateFileName != null)
			{
				PlayerUpdateTokenData.singleton.AddEquipedItemToContainer(playerStatSO.farmJsonUpdateFileName);
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		#region Hero Equipment Inventory Data Fetch
		
		public IEnumerator FetchTokenDataToContainer()
		{
			tokenFetched.Clear();
			
			if (DataGateWay.singleton != null && equipmentContainer != null)
			{
				BinaryFormatter bf = new BinaryFormatter();
				var data = new AllNftName { };
				if (File.Exists(Application.persistentDataPath  + GlobalName.tokenPath + GlobalName.allNFTFileName))
				{
					FileStream file = File.Open(Application.persistentDataPath  + GlobalName.tokenPath + GlobalName.allNFTFileName, FileMode.Open);
					JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), data);
					file.Close();
				}
				

				if (data != null && data.name != null)
				{
					for (int i = 0; i < data.name.Count; i++)
					{
						try
						{
							if (File.Exists(Application.persistentDataPath + GlobalName.tokenPath + data.name[i]))
							{
								LoadTokenFile(data.name[i], GlobalName.tokenPath, i);
							}
							
						}
						catch (Exception e)
						{
							Console.WriteLine(e);
							throw;
						}
					}
				}

				
				
				
				
				



				yield return null;
				
				SO_WeaponItem temp;
        
				for (int i = 0; i < tokenFetched.Count; i++)
				{
					if (tokenFetched.TryGetValue(i, out temp))
					{
						if (equipmentContainer != null)
						{
							equipmentContainer.AddItemWeapon(temp);
						}
        
        				
					}
				}
				
				StartCoroutine(UnlockWeapon.FetchUnlockableListData(delegate(List<AllUnlockableNftName> result)
				{
					if (result.Count > 0)
					{
						for (int i = 0; i < result.Count; i++)
						{
							try
							{
								if (result[i].isMinted == "false")
								{
									if (File.Exists(Application.persistentDataPath  + GlobalName.tokenPath + result[i].name))
									{
										equipmentContainer.AddItemWeapon(LoadUnlockableTokenFile(result[i].name, GlobalName.tokenPath));
									}
									
								}
										
								
							}
							catch (Exception e)
							{
								Console.WriteLine(e);
								throw;
							}
						}
					}
					
					
				}));
				
				
				// if (m_dataAllUnlockableName.name != null)
				// {
				// 	for (int i = 0; i < m_dataAllUnlockableName.name.Count; i++)
				// 	{
				// 		try
				// 		{
				// 			
				// 			equipmentContainer.AddItemWeapon(LoadUnlockableTokenFile(m_dataAllUnlockableName.name[i], GlobalName.tokenPath));
				// 		}
				// 		catch (Exception e)
				// 		{
				// 			Console.WriteLine(e);
				// 			throw;
				// 		}
				// 	}
				// }
				
			}
		}
		
		private void LoadTokenFile(string _fileName , string path, int i)
        	{
	            var tokenSO = ScriptableObject.CreateInstance<Token>();
        		
        		BinaryFormatter bf = new BinaryFormatter();
        		if (File.Exists(Application.persistentDataPath  + path + _fileName))
        		{
        			FileStream file = File.Open(Application.persistentDataPath  + path + _fileName, FileMode.Open);
        
        			JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), tokenSO);
        			file.Close();
        		}
        		
        		var itemSO = ScriptableObject.CreateInstance<SO_WeaponItem>();
        		itemSO.slotPrefabUI = DataGateWay.singleton.slotPrefab_Ui;
                itemSO.tokenId = tokenSO.Id;
        		itemSO.name = tokenSO.SystemName;
        		itemSO.description = tokenSO.Description;
        		itemSO.attackPrefab_UI = DataGateWay.singleton.attackDamage_Ui;
        		itemSO.defensePrefab_UI = DataGateWay.singleton.defense_Ui;
        		itemSO.farmPrefab_UI = DataGateWay.singleton.farm_Ui;
        		
        		var a = tokenSO.GetTokenDefinition();
        		foreach (var attr in a.attributes)
        		{
        			switch (attr.name)
        			{
        				case "Attack Damage":
        					itemSO.weaponAttack = int.Parse(attr.value);
        					break;
        				case "Defense":
        					itemSO.weaponDefense = int.Parse(attr.value);
        					break;
        				case "Farm Power":
        					itemSO.weaponFarm = int.Parse(attr.value);
        					break;
        				case "Level":
        					itemSO.weaponLevel = int.Parse(attr.value);
        					break;
        				case "Price":
        							
        					break;
        			}
        		}
        		
        		itemSO.itemIcon = DataGateWay.singleton.sprite[UnityEngine.Random.Range(0, 3)];
        		
        
        		if (itemSO.name == "Axe" || itemSO.name == "Sword")
        		{
        			itemSO.inventoryItemType = InventoryItemType.ActionWeapon;
        		}
        		if(itemSO.name == "Hammer" || itemSO.name == "Blade")
        		{
        			itemSO.inventoryItemType = InventoryItemType.Farmweapon;
        		}
                
                
                tokenFetched.Add(i, itemSO);

            }
		
		
		private SO_WeaponItem LoadUnlockableTokenFile(string _fileName , string path)
        	{
	            var tokenSO = ScriptableObject.CreateInstance<Token>();
        		
        		BinaryFormatter bf = new BinaryFormatter();
        		if (File.Exists(Application.persistentDataPath  + path + _fileName))
        		{
        			FileStream file = File.Open(Application.persistentDataPath  + path + _fileName, FileMode.Open);
        
        			JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), tokenSO);
        			file.Close();
        		}
        		
        		var itemSO = ScriptableObject.CreateInstance<SO_WeaponItem>();
        		itemSO.slotPrefabUI = DataGateWay.singleton.slotPrefab_Ui;
                itemSO.tokenId = tokenSO.Id;
        		itemSO.name = tokenSO.SystemName;
        		itemSO.description = tokenSO.Description;
        		itemSO.attackPrefab_UI = DataGateWay.singleton.attackDamage_Ui;
        		itemSO.defensePrefab_UI = DataGateWay.singleton.defense_Ui;
        		itemSO.farmPrefab_UI = DataGateWay.singleton.farm_Ui;
        		
        		var a = tokenSO.GetTokenDefinition();
        		foreach (var attr in a.attributes)
        		{
        			switch (attr.name)
        			{
        				case "Attack Damage":
        					itemSO.weaponAttack = int.Parse(attr.value);
        					break;
        				case "Defense":
        					itemSO.weaponDefense = int.Parse(attr.value);
        					break;
        				case "Farm Power":
        					itemSO.weaponFarm = int.Parse(attr.value);
        					break;
        				case "Level":
        					itemSO.weaponLevel = int.Parse(attr.value);
        					break;
        				case "Price":
        							
        					break;
        			}
        		}
        		
        		itemSO.itemIcon = DataGateWay.singleton.sprite[UnityEngine.Random.Range(0, 3)];
        		
        
        		if (itemSO.name == "Axe" || itemSO.name == "Sword")
        		{
        			itemSO.inventoryItemType = InventoryItemType.ActionWeapon;
        		}
        		if(itemSO.name == "Hammer" || itemSO.name == "Blade")
        		{
        			itemSO.inventoryItemType = InventoryItemType.Farmweapon;
        		}


                return itemSO;

            }
		
		#endregion
		
		
		
		
		
		#endregion
        
    }
    
}


