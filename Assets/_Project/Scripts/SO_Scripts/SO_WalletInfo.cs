using System;
using UnityEngine;

namespace TD
{
	[CreateAssetMenu(fileName = "Wallet Data", menuName = "Scriptable Objects/Wallet")]
    public class SO_WalletInfo : ScriptableObject
    {

	    public WalletLoginType walletLoginType;
	    [Header("---- VENLY ----")] 
	    public string v_walletAddress;
	    public string v_walletChain;
	    public string v_walletId;
	    
	    [Header("---- METAMASK ----")] 
	    public string m_walletAddress;
	    public string m_walletChain;

    }



    [Serializable]
    public enum WalletLoginType
    {
	    NONE,
	    Metamask,
	    Venly
    }
    
}


