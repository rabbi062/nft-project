using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace TD.Utilities
{
	[CreateAssetMenu(menuName = "Utilities/ObjectPooler", fileName = "ObjectPooler")]
    public class ObjectPoolAsset : ScriptableObject
    {
	#region Variable

	public Pool[] pools;
	[System.NonSerialized] 
	private Dictionary<PoolObjectType, Pool> poolDict = new Dictionary<PoolObjectType, Pool>();

	[System.NonSerialized] 
	private Transform parentTransform;

	public bool prewarm;

	#endregion

	#region Function
	//------------------------------------------------------------------------------------------------------------------
	public void Init()
	{
		parentTransform = new GameObject("Object Pool").transform;

		for (int i = 0; i < pools.Length; i++)
		{
			poolDict.Add(pools[i].poolType, pools[i]);
			if (prewarm)
			{
				pools[i].PrewarmObject(parentTransform);
			}
		}
	}
	
	
	//------------------------------------------------------------------------------------------------------------------
	public GameObject GetObject(PoolObjectType id)
	{
		poolDict.TryGetValue(id, out Pool value);
		return value.GetObject(parentTransform);
	}

	#endregion

    }
	
    //------------------------------------------------------------------------------------------------------------------
    [System.Serializable]
    public class Pool
    {
	    public PoolObjectType poolType;
	    public GameObject prefab;
	    public int budget;

	    [System.NonSerialized] 
	    private List<GameObject> createdObjects = new List<GameObject>();

	    [System.NonSerialized] 
	    private int index;
		
	    
	    //--------------------------------------------------------------------------------------------------------------

	    public GameObject GetObject(Transform parent)
	    {
		    GameObject retVal = null;
		    if (createdObjects.Count < budget)
		    {
			    GameObject go = GameObject.Instantiate(prefab) as GameObject;
			    go.transform.parent = parent;
			    createdObjects.Add(go);
			    retVal = go;
		    }
		    else
		    {
			    retVal = createdObjects[index];
			    index++;
			    if (index > createdObjects.Count - 1)
			    {
				    index = 0;
			    }
		    }
		    retVal.SetActive(false);
		    return retVal;
	    }
	    
	    //--------------------------------------------------------------------------------------------------------------

	    public void PrewarmObject(Transform parent)
	    {
		    for (int i = 0; i < budget; i++)
		    {
			    GameObject go = GameObject.Instantiate(prefab) as GameObject;
			    go.SetActive(false);
			    go.transform.parent = parent;
			    createdObjects.Add(go);
		    }
	    }
	    

    }

    public enum PoolObjectType
    {
	    GrassFloorHitFx,
	    SandFloorHitFx,
	    StoneFloorHitFx,
	    LavaFloorHitFx,
	    SnowFloorHitFx,
	    TileVanishFx,
	    ShopPrefab,
	    NONE
    }
    
}//NAMESPACE


