using UnityEngine;

namespace TD.Utilities
{
    public static class ObjectPool
    {


        private static ObjectPoolAsset poolAsset;

        public static ObjectPoolAsset ObjectPooler
        {
            get
            {
                if (poolAsset == null)
                {
                    poolAsset = Resources.Load("ObjectPooler") as ObjectPoolAsset;
                    poolAsset.Init();
                }

                return poolAsset;
            }
        }

        public static GameObject GetObject(PoolObjectType id)
        {
            return ObjectPooler.GetObject(id);
        }


    }
}


