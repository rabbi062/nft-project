using System;
using UnityEngine;

namespace TD.NFT
{
    public abstract class SingletonMonobehaviour<T> : MonoBehaviour where T:MonoBehaviour
    {

        private static T singleton;

        public static T Singleton
        {
            get
            {
                return singleton;
            }
        }

        protected void Awake()
        {
            if (singleton == null)
            {
                singleton = this as T;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}


