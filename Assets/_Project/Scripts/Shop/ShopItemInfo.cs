using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

namespace TD
{
    public class ShopItemInfo : MonoBehaviour, IPointerClickHandler
    {
		#region Variable
		[Header("---- Setting ----")] 
		[SerializeField] private GameObject focus;




		[HideInInspector] public string name;
		[HideInInspector] public string offerId;
		[HideInInspector] public string status;
		[HideInInspector] public int price;

		#endregion

		#region Function

		private void OnEnable()
		{
			if (focus == null)
			{
				focus = this.transform.GetChild(2).gameObject;
			}
			ResetAllShopFocus();
		}

		private void OnDisable()
		{
			ResetAllShopFocus();
		}


		public void OnPointerClick(PointerEventData eventData)
		{
			ResetAllShopFocus();
			focus.SetActive(true);
			
			
			var nftShopManager = Object.FindObjectOfType<NFTShop>();
			nftShopManager.Offer_Id = offerId;
			nftShopManager.Offer_Price = price;
			nftShopManager.Offer_Name = name;
			
			var uiShopManager = Object.FindObjectOfType<UI_ShopManager>();
			uiShopManager.ShowInfoOnClick();
		}


		void ResetAllShopFocus()
		{
			ShopItemInfo[] allButtons = GameObject.FindObjectsOfType<ShopItemInfo>();
			foreach (var shopButton in allButtons)
			{
				shopButton.focus.SetActive(false);
			}
		}

		#endregion

	
    }
}


