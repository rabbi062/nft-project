using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TD
{
    public class UI_HeroManager : MonoBehaviour
    {
	#region Variable
	
	[SerializeField] private List<GameObject> heroContainer;
	[SerializeField] private int totalHero;
	
	[Header("---- UI Button ----")] 
	[SerializeField] private Button _leftSwapButton;
	[SerializeField] private Button _rightSwapButton;
	
	
	[Header("---- UI ----")] 
	[SerializeField] private TMP_Text hero_Name;
	[SerializeField] private TMP_Text hero_Level;
	
	
	[Header("---- Setting ----")] 
	[SerializeField] private Transform heroParent;

	[SerializeField] private Ease ease = Ease.OutSine;
	
	private SO_HeroContainer heroContainerSO = null;
	private SOPlayerStat playerStatSO = null;
	
	
	
	
	private float tweenDuration = 0.5f;

	private Vector3 leftSwapPos = new Vector3(-1500f, 0f, 0f);
	private Vector3 midPos = new Vector3(0f, 0f, 0f);
	private Vector3 rightSwapPos = new Vector3(-1500f, 0f, 0f);

	private int heroIndex;
	private SO_Hero heroVal;
	public Dictionary<int, SO_Hero> heroes = new Dictionary<int, SO_Hero>();

	private int activeHeroId;
	
	
	
	
	#endregion

	#region Function

	private void Awake()
	{
		if (!PlayerPrefs.HasKey(GlobalName.HERO_SELECTION_INDEX))
		{
			PlayerPrefsUtility.SetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0);
		}

		if (heroContainerSO == null)
		{
			heroContainerSO = Resources.Load<SO_HeroContainer>("Hero Container");
		}
		
		if (playerStatSO == null)
		{
			playerStatSO = Resources.Load<SOPlayerStat>("Player/so_playerStat");
		}
		
	}

	private void Start()
	{
		
		_leftSwapButton.onClick.RemoveAllListeners();
		_leftSwapButton.onClick.AddListener(() =>
		{
			if (heroContainerSO == null) return;
			
			PreviousHero();
			
		});
		
		
		_rightSwapButton.onClick.RemoveAllListeners();
		_rightSwapButton.onClick.AddListener(() =>
		{
			if (heroContainerSO == null) return;
			
			NextHero();
			
		});
		
	}


	private void OnEnable()
	{
		if (heroContainerSO == null) return;
		
		
		totalHero = heroContainerSO.HeroContainer.Count;
		heroIndex = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0);
		for (int i = 0; i < heroContainerSO.HeroContainer.Count; i++)
		{
			heroes.Add(i, heroContainerSO.HeroContainer[i].item);
			
			StartCoroutine(GenerateHero(heroContainerSO.HeroContainer[i].item, callback =>
			{
				if (callback == "SUCCESS")
				{
					DefaultHero();
				}
			}));
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	IEnumerator GenerateHero(SO_Hero hero, UnityAction<string> callback)
	{
		GameObject obj = Instantiate(hero.slotPrefabUI);
		obj.GetComponent<Image>().sprite = hero.itemIcon;
		obj.transform.SetParent(heroParent, false);
		heroContainer.Add(obj);
		yield return null;
		callback?.Invoke("SUCCESS");

	}
	
	
	//------------------------------------------------------------------------------------------------------------------
	private void DefaultHero()
	{
		heroIndex = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0);
		
		if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0) == totalHero - 1)
		{
			_leftSwapButton.interactable = true;
			_rightSwapButton.interactable = false;
		}
		else if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0) == 0)
		{
			_leftSwapButton.interactable = false;
			_rightSwapButton.interactable = true;
		}
		else
		{
			_leftSwapButton.interactable = true;
			_rightSwapButton.interactable = true;
		}

		if (totalHero == 1)
		{
			_leftSwapButton.interactable = false;
			_rightSwapButton.interactable = false;
		}

		UpdateHeroUIInfo();

	}

	//------------------------------------------------------------------------------------------------------------------
	private void NextHero()
	{
		heroIndex = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0) + 1;
		PlayerPrefsUtility.SetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, heroIndex);
		
		if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0) == totalHero - 1)
		{
			_leftSwapButton.interactable = true;
			_rightSwapButton.interactable = false;
		}
		else
		{
			_leftSwapButton.interactable = true;
			_rightSwapButton.interactable = true;
		}
		
		UpdateHeroUIInfo();
		
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private void PreviousHero()
	{
		heroIndex = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0) - 1;
		PlayerPrefsUtility.SetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, heroIndex);
		
		if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.HERO_SELECTION_INDEX, 0) == 0)
		{
			_leftSwapButton.interactable = false;
			_rightSwapButton.interactable = true;
		}
		else
		{
			_leftSwapButton.interactable = true;
			_rightSwapButton.interactable = true;
		}
		
		UpdateHeroUIInfo();
	}
	
	
	//------------------------------------------------------------------------------------------------------------------

	void UpdateHeroUIInfo()
	{
		foreach (var heroObj in heroContainer)
		{
			heroObj.transform.localPosition = leftSwapPos;
		}
		heroContainer[heroIndex].transform.DOLocalMove(midPos, tweenDuration).SetEase(ease);

		if (heroes.TryGetValue(heroIndex, out heroVal))
		{
			hero_Name.text = heroVal.name.ToString();
			hero_Level.text = heroVal.heroLevel.ToString();
			playerStatSO.hp = heroVal.heroHp;
		}
	}

	#endregion
        
    }
}


