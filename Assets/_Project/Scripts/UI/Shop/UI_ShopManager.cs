using System;
using System.Collections.Generic;
using HeathenEngineering.BGSDK.API;
using HeathenEngineering.BGSDK.DataModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using WalletConnectSharp.Unity;
using Object = UnityEngine.Object;

namespace TD
{
    public class UI_ShopManager : UIBaseSC
    {
	#region Variable

	[Header("---- CREATE WALLET ----")] 
	[SerializeField] private TMP_InputField walletID_CreateWallet;
	[SerializeField] private TMP_InputField pin_CreateWallet;
	[SerializeField] private Button createWallet_Button;
	
	[Header("---- IMPORT WALLET ----")]
	[SerializeField] private Button importMetamaskWallet_Button;

	[Space(10)]
	[SerializeField] private Button close_walletPanel;
	

	[Space(10)] 
	[SerializeField] private TMP_Text walletAddress;
	
	
	[Header("---- Shop Detail Setting ----")] 
	[SerializeField] private TMP_Text name_NftShopItem;
	[SerializeField] private TMP_Text price_NftShopItem;
	[SerializeField] private TMP_Text finalBuyPrice_Text;


	[SerializeField] private Button buy_NFTItemBtn;
	[SerializeField] private Button finalBuy_NFTItemBtn;
	[SerializeField] private Button cancel_NFTItemBtn;

	[Header("---- Successful Buy ----")]
	[SerializeField]
	private Button done_SuccessfulBuyBtn;
	
	
	[Header("Results")]
	public List<GetWalletByIdentifierResult.WalletIdentifier> walletData;
	

	private const string walletPanel = "Wallet";
	private const string finalBuyPanel = "Final Buy Panel";
	private const string successfulBuyPanel = "Successful Buy";
	
	
	
	#endregion

	#region Function
	//------------------------------------------------------------------------------------------------------------------
	private void Start()
	{

		CloseMenu(finalBuyPanel);
		
		if (!string.IsNullOrEmpty(PlayerPrefs.GetString("Wallet Address")) || !string.IsNullOrEmpty(PlayerPrefs.GetString("Wallet Id")))
		{
			CloseMenu(walletPanel);
		}
		else
		{
			ShowMenu(walletPanel);
		}
		
		
		
		close_walletPanel.onClick.RemoveAllListeners();
		close_walletPanel.onClick.AddListener(() =>
		{
			CloseMenu(walletPanel);
		});
		
		//-----------------------------------  Create Wallet
		createWallet_Button.onClick.RemoveAllListeners();
		createWallet_Button.onClick.AddListener(() =>
		{
			Create_Wallet();
			createWallet_Button.gameObject.SetActive(false);
		});
		
		//----------------------------------  Import Wallet
		
		

		//-----------------------------------   Buy Button
		
		buy_NFTItemBtn.onClick.RemoveAllListeners();
		buy_NFTItemBtn.onClick.AddListener(() =>
		{
			ShowMenu(finalBuyPanel);
		});
		
		cancel_NFTItemBtn.onClick.RemoveAllListeners();
		cancel_NFTItemBtn.onClick.AddListener(() =>
		{
			CloseMenu(finalBuyPanel);
		});
		
		finalBuy_NFTItemBtn.onClick.RemoveAllListeners();
		finalBuy_NFTItemBtn.onClick.AddListener(() =>
		{
			finalBuy_NFTItemBtn.gameObject.SetActive(false);
			cancel_NFTItemBtn.gameObject.SetActive(false);
			// Buy item Final
			StartCoroutine(NFTShop.singleton.BuyNFtFromShop(str =>
			{
				if (str =="SUCCESS")
				{
					ShowMenu(successfulBuyPanel);
				}
				
			}));

		});
		
		done_SuccessfulBuyBtn.onClick.RemoveAllListeners();
		done_SuccessfulBuyBtn.onClick.AddListener(() =>
		{
			ResetInfo();
			CloseMenu(successfulBuyPanel);
		});
	}

	private void OnEnable()
	{
		buy_NFTItemBtn.interactable = false;

		 WalletConnect.OnMetamaskConnected += Import_MetamaskWallet;
	}

	private void OnDisable()
	{
		 WalletConnect.OnMetamaskConnected -= Import_MetamaskWallet;
	}


	private void Update()
	{
		walletAddress.text = PlayerPrefs.GetString("Wallet Address");
	}

	//------------------------------------------------------------------------------------------------------------------
	#region CREATE WALLET

	void Create_Wallet()
	{
		if (!string.IsNullOrEmpty(walletID_CreateWallet.text)
		    && !string.IsNullOrEmpty(pin_CreateWallet.text))
		{
			PlayerPrefs.SetString("Wallet Id", walletID_CreateWallet.text);
			StartCoroutine(Server.Wallets.Create(pin_CreateWallet.text, walletID_CreateWallet.text, SecretType.MATIC, HandleCreateWalletResult));
		}
		else
		{
			Debug.LogWarning("You must provide a wallet ID and Pin in order to create a new wallet.");
		}
		
	}
	private void HandleCreateWalletResult(ListWalletResult walletResult)
	{
		if (!walletResult.hasError)
		{
			this.Wait(.5f, () =>
			{
				string s = PlayerPrefs.GetString("Wallet Id");
				StartCoroutine(Server.Wallets.GetByIdentifier(s, result =>
				{
					walletData = result.result;
					PlayerPrefs.SetString("Wallet Address", walletData[0].address);
					this.Wait(.5f, () =>
					{
						CloseMenu(walletPanel);
					});

				}));
			});
		}
		else
		{
			Debug.Log("Create Wallets Responce:\nHas Error: " + walletResult.hasError + "\nMessage: " + walletResult.message + "\nCode:" + walletResult.httpCode);
		}
	}
	
	// private void HandleListWalletResults(Server.Wallets.GetWalletByIdentifierResult walletResult)
	// {
	// 	if (!walletResult.hasError)
	// 	{
	// 		
	// 		walletData = walletResult.result;
	// 		
	// 		foreach (var wData in walletData)
	// 		{
	// 			if (wData.id == "user-id=user1")
	// 			{
	// 				Debug.Log(wData.address);
	// 			}
	// 			
	// 		}
	// 		
	// 		
	// 		// this.Wait(10f, () =>
	// 		// {
	// 		// 	foreach (var wData in walletData)
	// 		// 	{
	// 		// 		Debug.Log(wData.address);
	// 		// 		
	// 		// 		if (wData.id == PlayerPrefs.GetString("Wallet Id"))
	// 		// 		{
	// 		// 			Debug.Log(wData.address);
	// 		// 			PlayerPrefs.SetString("Wallet Address", wData.address);
	// 		// 		}
	// 		// 	}
	// 		// 	//CloseMenu(walletPanel);
	// 		// });
	// 		
	// 		
	// 	}
	// 	else
	// 	{
	// 		Debug.Log("List Wallets Responce:\nHas Error: " + walletResult.hasError + "\nMessage: " + walletResult.message + "\nCode:" + walletResult.httpCode);
	// 	}
	// }
	
	#endregion
	//------------------------------------------------------------------------------------------------------------------
	public void Import_MetamaskWallet()
	{

		CloseMenu(walletPanel);
		string wAddress = PlayerPrefs.GetString("Account");
		PlayerPrefs.SetString("Wallet Address", wAddress);
		// PlayerPrefs.SetString("Wallet Address", walletAddress_ImporteWallet.text);
		// StartCoroutine(WalletExtra.ImportWallet(ImportWalletType.MATIC_PRIVATE_KEY ,int.Parse(pin_ImportWallet.text), privateKey_ImporteWallet.text, result =>
		// {
		// 	if (!result.hasError)
		// 	{
		// 		this.Wait(3f, () =>
		// 		{
		// 			CloseMenu(walletPanel);
		// 		});
		// 	}
		// 		
		// }));
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public void ShowInfoOnClick()
	{
		buy_NFTItemBtn.interactable = true;
		
		// Name
		if (!string.IsNullOrEmpty(NFTShop.singleton.Offer_Name))
		{
			name_NftShopItem.text = NFTShop.singleton.Offer_Name;
		}
		else
		{
			name_NftShopItem.text = "";
		}
		
		//price
		if (!(NFTShop.singleton.Offer_Price == 0))
		{
			price_NftShopItem.text = NFTShop.singleton.Offer_Price.ToString();
			finalBuyPrice_Text.text = NFTShop.singleton.Offer_Price.ToString();
		}
		else
		{
			price_NftShopItem.text = "0";
			finalBuyPrice_Text.text = "0";
		}
		
	}

	public void ResetInfo()
	{
		name_NftShopItem.text = "";
		price_NftShopItem.text = "0";
		finalBuyPrice_Text.text = "0";
	}
	
	
	

	#endregion

    }
}


