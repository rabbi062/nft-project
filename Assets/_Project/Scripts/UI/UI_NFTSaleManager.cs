using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.API;
using HeathenEngineering.BGSDK.DataModel;
using HeathenEngineering.BGSDK.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TD
{
    public class UI_NFTSaleManager : UIBaseSC
    {
	#region Variable



	[Header("---- Button ----")] 
	[SerializeField] private Button SaleOffer_Button;
	[SerializeField] private Button mint_Button;
	[SerializeField] private Button close_priceParentPanel_button;
	[SerializeField] private Button close_priceNewPanel_button;

	[Header("---- Final Sale Button ----")] 
	[SerializeField] private Button final_ParentSale_Button;
	[SerializeField] private Button final_NewSale_Button;
	
	
	[Header("---- Parent Nft  ----")] 
	[SerializeField] private TMP_InputField parent_Pin_InputField;
	[SerializeField] private TMP_InputField parent_Price_InputField;
	[SerializeField] private TMP_Text parent_WalletAddress_Text;
	[SerializeField] private TMP_Text parent_WalletChain_Text;
	[SerializeField] private TMP_Text parent_NftName_Text;
	
	
	[Header("---- New Nft  ----")] 
	// [SerializeField] private TMP_InputField new_Pin_InputField;
	// [SerializeField] private TMP_InputField new_Price_InputField;
	[SerializeField] private TMP_Text new_WalletAddress_Text;
	[SerializeField] private TMP_Text new_WalletChain_Text;
	[SerializeField] private TMP_Text new_NftName_Text;

	[Header("------------------------------------------------------------------")]
	[Header("---- Panel Info ----")]
	[SerializeField] private TMP_Text nftName_Text;
	[SerializeField] private TMP_Text parentNftLevel_Text;
	[SerializeField] private TMP_Text newNftLevel_Text;
	[SerializeField] private TMP_Text finalSaleLoading_Text;
	
	

	private const string priceSetParentPanel = "Price Set Parent Panel";
	private const string priceSetNewPanel = "Price Set New Panel";
	private const string saleLoadingPanel = "Sale Loading";
	
	
	//--------------------
	
	string OfferId = "";
	private string _pinCode;
	private string tokenId;
	private string contractAddress;
	private string price;

	public Contract contract;
	//private Token createNewTokenDef;
	private Token parentTokenDef;
	#endregion

	#region Function

	private void Start()
	{
		
		
		
		SaleOffer_Button.onClick.RemoveAllListeners();
		SaleOffer_Button.onClick.AddListener(() =>
		{
			ShowMenu(priceSetParentPanel);
		});
		
		mint_Button.onClick.RemoveAllListeners();
		mint_Button.onClick.AddListener(() =>
		{
			ShowMenu(priceSetNewPanel);
		});
		
		close_priceParentPanel_button.onClick.RemoveAllListeners();
		close_priceParentPanel_button.onClick.AddListener(() =>
		{
			CloseMenu(priceSetParentPanel);
		});
		
		close_priceNewPanel_button.onClick.RemoveAllListeners();
		close_priceNewPanel_button.onClick.AddListener(() =>
		{
			CloseMenu(priceSetNewPanel);
		});
		
		parent_Pin_InputField.onValueChanged.AddListener(delegate(string arg0) { InteractionParentSale(true);});
		parent_Price_InputField.onValueChanged.AddListener(delegate(string arg0) { InteractionParentSale(true);});
		
		InteractionNewSale(true);
		
		
		
		final_ParentSale_Button.onClick.RemoveAllListeners();
		final_ParentSale_Button.onClick.AddListener(() =>
		{
			StartSaleOffer();
		});
		final_NewSale_Button.onClick.RemoveAllListeners();
		final_NewSale_Button.onClick.AddListener(() =>
		{
			bool isParent = false;
			bool hasChild = false;
			string childCount = null;
			
			var a = parentTokenDef.GetTokenDefinition();
			foreach (var attr in a.attributes)
			{
				if (attr.name == "Parent")
				{
					isParent = true;
				}
				if (attr.name == "Child")
				{
					hasChild = true;
				}

				if (attr.name == "Child Count")
				{
					childCount = attr.value;
				}
			}
			// No Need
			if (hasChild && !isParent)
			{
				ShowMenu(saleLoadingPanel);
				finalSaleLoading_Text.text = "Already Minted From Parent";
				this.Wait(3f, () =>
				{
					MDisableAllScreens();
				});
			}
			// No Need
			if (isParent && childCount == "1")
			{
				ShowMenu(saleLoadingPanel);
				finalSaleLoading_Text.text = "Parent already have child minted";
				this.Wait(3f, () =>
				{
					MDisableAllScreens();
				});
			}

			if (isParent && !hasChild && (childCount == "0" || childCount == null))
			{
				StartCreateTokenType();
			}
			
			
		});
		
		

	}


	private void InteractionParentSale(bool t)
	{
		if (t)
		{
			final_ParentSale_Button.interactable = true;
		}
		else
		{
			final_ParentSale_Button.interactable = false;
		}
		
	}
	
	private void InteractionNewSale(bool t)
	{
		if (t)
		{
			final_NewSale_Button.interactable = true;
		}
		else
		{
			final_NewSale_Button.interactable = false;
		}
		
	}
	

	private void OnEnable()
	{
		InteractionParentSale(false);
		mint_Button.interactable = true;
		SaleOffer_Button.interactable = false;
		
		FetchDataJsonToSaleParent();
		
	}



	void FetchDataJsonToSaleParent()
	{
		
		// Fetch Data
		string _fileName;
		var tokenSO = ScriptableObject.CreateInstance<Token>();
		if (UI_HeroEquipmentInventoryManager.singleton != null && UI_HeroEquipmentInventoryManager.singleton.WhichTonenJsonName != null)
		{
			_fileName = UI_HeroEquipmentInventoryManager.singleton.WhichTonenJsonName;
		}
		else
		{
			return;
		}
				
		BinaryFormatter bf = new BinaryFormatter();
		if (File.Exists(Application.persistentDataPath  + GlobalName.tokenPath + _fileName))
		{
			FileStream file = File.Open(Application.persistentDataPath  + GlobalName.tokenPath + _fileName, FileMode.Open);
        
			JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), tokenSO);
			file.Close();
		}

		parentTokenDef = tokenSO;
		//FetchDataJsonToSaleNew(tokenSO.SystemName + tokenSO.Description + "_" + tokenSO.Id);
		// Show Stat
		//Nft Name
		nftName_Text.text = tokenSO.SystemName;
		parent_NftName_Text.text = tokenSO.SystemName;

		tokenId = tokenSO.Id;
		contractAddress = tokenSO.Address;
		

		// Parent Nft Level
		var a = tokenSO.GetTokenDefinition();
		foreach (var attr in a.attributes)
		{
			switch (attr.name)
			{
				case "Attack Damage":
					//itemSO.weaponAttack = int.Parse(attr.value);
					break;
				case "Defense":
					//itemSO.weaponDefense = int.Parse(attr.value);
					break;
				case "Farm Power":
					//itemSO.weaponFarm = int.Parse(attr.value);
					break;
				case "Level":
					parentNftLevel_Text.text = attr.value;
					break;
				case "Price":
        							
					break;
				case "tokenTypeId":
					SaleOffer_Button.interactable = true;
					break;
			}

			if (attr.name == "mintNumber")
			{
				mint_Button.interactable = false;
			}
			else
			{
				mint_Button.interactable = true;
			}

		}

		// Wallet Info
		parent_WalletAddress_Text.text = PlayerPrefs.GetString(GlobalName.currentUserWalletAddress);
		parent_WalletChain_Text.text = PlayerPrefs.GetString(GlobalName.currentUserWalletChain);

		// Wallet Info
		new_WalletAddress_Text.text = PlayerPrefs.GetString(GlobalName.currentUserWalletAddress);
		new_WalletChain_Text.text = PlayerPrefs.GetString(GlobalName.currentUserWalletChain);



	}

	// void FetchDataJsonToSaleNew( string _fileNameNew)
	// {
	// 	if (!string.IsNullOrEmpty(_fileNameNew))
	// 	{
	// 		mint_Button.interactable = true;
	// 	}
	// 	
	// 	var tokenSONew = ScriptableObject.CreateInstance<Token>();
	// 	BinaryFormatter bf = new BinaryFormatter();
	// 	if (File.Exists(Application.persistentDataPath  + GlobalName.tokenPath + _fileNameNew))
	// 	{
	// 		FileStream file = File.Open(Application.persistentDataPath  + GlobalName.tokenPath + _fileNameNew, FileMode.Open);
 //        
	// 		JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), tokenSONew);
	// 		file.Close();
	// 	}
	//
	// 	createNewTokenDef = tokenSONew;
	// 	new_NftName_Text.text = tokenSONew.SystemName;
	// 	
	// 	
	// 	var a = tokenSONew.GetTokenDefinition();
	// 	foreach (var attr in a.attributes)
	// 	{
	// 		switch (attr.name)
	// 		{
	// 			case "Attack Damage":
	// 				//itemSO.weaponAttack = int.Parse(attr.value);
	// 				break;
	// 			case "Defense":
	// 				//itemSO.weaponDefense = int.Parse(attr.value);
	// 				break;
	// 			case "Farm Power":
	// 				//itemSO.weaponFarm = int.Parse(attr.value);
	// 				break;
	// 			case "Level":
	// 				newNftLevel_Text.text = attr.value;
	// 				break;
	// 			case "Price":
 //        							
	// 				break;
	// 		}
	// 	}
	// 	
	// 	
	// 	
	// 	// Wallet Info
	// 	new_WalletAddress_Text.text = PlayerPrefs.GetString(GlobalName.currentUserWalletAddress);
	// 	new_WalletChain_Text.text = PlayerPrefs.GetString(GlobalName.currentUserWalletChain);
	// }
	
	

	#region Sale Offer
	void StartSaleOffer()
	{
		if (string.IsNullOrEmpty(parent_Pin_InputField.text) || string.IsNullOrEmpty(parent_Price_InputField.text)) return;

		price = parent_Price_InputField.text;
		_pinCode = parent_Pin_InputField.text;
		
		ShowMenu(saleLoadingPanel);
		StartCoroutine(Server.Market.CreateOffer(tokenId, contractAddress,
			PlayerPrefs.GetString(GlobalName.currentUserWalletAddress),
			PlayerPrefs.GetString(GlobalName.currentUserWalletChain), price, CreateOfferResult));

	}
	
	void CreateOfferResult(OfferResult result)
	{
		if (result != null && !result.hasError)
		{
			finalSaleLoading_Text.text = "CREATED Offer";
			OfferId = result.result.id;
			
			
			// if user use venly wallet
			StartCoroutine(Server.Market.SignOffer(PlayerPrefs.GetString(GlobalName.currentUserWalletId),
					result.result.dataToSign,
					_pinCode, PlayerPrefs.GetString(GlobalName.currentUserWalletChain), SignOfferResult));
			
			// If user use metamask wallet

			
		}
	}
	
	void SignOfferResult(OfferSignatureResult result)
	{
		if (result != null && !result.hasError)
		{
			finalSaleLoading_Text.text = "Signed Offer";
			StartCoroutine(Server.Market.Sign(result.result.signature, OfferId, S =>
			{
				if (S == "SUCCESS")
				{
					finalSaleLoading_Text.text = "SUCCESS";
					this.Wait(2f, () =>
					{
						MDisableAllScreens();
						//CloseMenu(saleLoadingPanel);
					});
					
				}
			}));
		}
		
	}
	

	#endregion


	#region Mint NFT

	void StartCreateTokenType()
	{
		if (parentTokenDef == null) return;
		
		int tokenTypeId = 0;
		ShowMenu(saleLoadingPanel);

		string[] walletAddress = new[] {PlayerPrefs.GetString(GlobalName.currentUserWalletAddress)};
		// StartCoroutine(Server.Tokens.UpdateTokenTypeMetadata(parentTokenDef, contract, metadataResult =>
		// {
		// 	Debug.Log(metadataResult.message);
		// }));

		
		finalSaleLoading_Text.text = "Starting To Minting";
		
		StartCoroutine(Server.Tokens.CreateTokenType(parentTokenDef, contract, result =>
		{
			Debug.Log(parentTokenDef.SystemName + parentTokenDef.Description);
			
			
			
			
			finalSaleLoading_Text.text = "Successful create nft";
			 if (!(result.message == "ERROR"))
			 {
			 	tokenTypeId = int.Parse(result.result.id);
			    StartCoroutine(UnlockWeapon.MakeUnlockableWeaponDataMintedTrue(parentTokenDef.SystemName + parentTokenDef.Description,
				    res =>
				    {
					    if (res == "SUCCESS")
					    {
						    this.Wait(2f, () =>
						    {
							    StartCoroutine(Server.TokenMint.MintNonFungibleToken(tokenTypeId, 2339.ToString(), walletAddress, result =>
							    {
								    finalSaleLoading_Text.text = "Successful Mint NFT";
								    File.Delete(Application.persistentDataPath + GlobalName.tokenPath + (parentTokenDef.SystemName + parentTokenDef.Description));
								    this.Wait(5f, () =>
								    {
									    MDisableAllScreens();
									    //CloseMenu(saleLoadingPanel);
									    if (LevelManager.singleton != null)
									    {
										    LevelManager.singleton.LoadLevel("Import token");
									    }
								    });
			 			
			 			
							    }));
						    });
					    }
				    }));
			    
			    
			 	
			 }
		
		}));
	}

	#endregion
	
	

	#endregion
        
    }
}


