using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TD
{
    public class UIHomeMenuController : UIBaseSC
    {
	#region Variable

	[Header("---- Test Unlockable ----")] 
	[SerializeField] private Button unlockWeapon_Button;

	



	[Header("---- Button ----")] 
	[SerializeField] private Button heroEquipInventory_Button;
	[SerializeField] private Button back_HeroEquipInventory_Button;
	[SerializeField] private Button test_Button;
	[SerializeField] private Button back_test_Button;

	[SerializeField] private Button play;
	
	
	//------------------------------------------------------------------------------------------------------------------
	private const string heroEquipmentInventory_Panel = "Hero Equipment Inventory";
	private const string test_Panel = "Test";
	
	#endregion

	#region Function
	
	//------------------------------------------------------------------------------------------------------------------
	private void Start()
	{
		MDisableAllScreens();
		ButtonPress();
		SetupUnlockButtonState();

		
		
		
		

	}

	private void OnEnable()
	{
		PlayerPrefs.DeleteKey(GlobalName.FIRSTTIME_LEVEL5UNLOCKWEAPON);
		if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalName.FIRSTTIME_LEVEL5UNLOCKWEAPON, 0) == 0)
		{
			unlockWeapon_Button.interactable = true;
		}
		else
		{
			unlockWeapon_Button.interactable = false;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void SetupUnlockButtonState()
	{
		
		unlockWeapon_Button.onClick.RemoveAllListeners();
		unlockWeapon_Button.onClick.AddListener(() =>
		{
			// StartCoroutine(UnlockWeapon.MakeUnlockableWeaponDataMintedTrue("BladeBlade v-A1B1", s =>
			// {
			// 	
			// }));
			
			StartCoroutine(UnlockWeapon.GetUnlockableWeaponDataFromServer("A1B2", result =>
			{
				
				if (result == "SUCCESS")
				{
					Debug.Log("=== : " + PlayerPrefs.GetString("Unlockable Weapon Name", ""));
					StartCoroutine(UnlockWeapon.UpdateAllUnlockableNftNameList(PlayerPrefs.GetString("Unlockable Weapon Name", "")));
				}
				
			}));

			PlayerPrefsUtility.SetPlayerPrefsInt(GlobalName.FIRSTTIME_LEVEL5UNLOCKWEAPON, 1);
			unlockWeapon_Button.interactable = false;
		});
	}
	
	
	//------------------------------------------------------------------------------------------------------------------
	private void ButtonPress()
	{
		
		play.onClick.RemoveAllListeners();
		play.onClick.AddListener(() =>
		{
			if (LevelManager.singleton != null)
			{
				LevelManager.singleton.LoadLevel("GameScene");
			}
			
		});
		
		
		
		heroEquipInventory_Button.onClick.RemoveAllListeners();
		heroEquipInventory_Button.onClick.AddListener(() =>
		{
			ShowMenu(heroEquipmentInventory_Panel);
			
		});
		
		back_HeroEquipInventory_Button.onClick.RemoveAllListeners();
		back_HeroEquipInventory_Button.onClick.AddListener(() =>
		{
			CloseMenu(heroEquipmentInventory_Panel);
			
		});
		
		
		test_Button.onClick.RemoveAllListeners();
		test_Button.onClick.AddListener(() =>
		{
			ShowMenu(test_Panel);
			
		});
		
		back_test_Button.onClick.RemoveAllListeners();
		back_test_Button.onClick.AddListener(() =>
		{
			CloseMenu(test_Panel);
			
		});
	}

	#endregion
        
    }
    
    
    
}


