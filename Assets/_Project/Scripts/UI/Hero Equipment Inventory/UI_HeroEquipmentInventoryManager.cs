using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TD
{
    public class UI_HeroEquipmentInventoryManager : UIBaseSC
    {
	    
	    #region Singleton

	    public static UI_HeroEquipmentInventoryManager singleton;

	    private void Awake()
	    {
		    if (singleton == null)
		    {
			    singleton = this;
		    }
		    else
		    {
			    Destroy(this.gameObject);
		    }
	    }

	    #endregion
	    
	    
		#region Variable

		[Header("---- Item Slot ----")] 
		[SerializeField] private Transform slotParent;

		[Header("---- Setting ----")] 
		private Dictionary<InventorySlot, GameObject> itemsDisplayed = new Dictionary<InventorySlot, GameObject>();
		private SO_HeroEquipmentSlotContainer inventory;
		
		[Header("---- Info Data UI ----")]
		[SerializeField] private TMP_Text itemRankText;
		[SerializeField] private TMP_Text itemNameText;
		[SerializeField] private TMP_Text infoText;
		[SerializeField] private Image itemIcon;
		[SerializeField] private Button itemDetailPanelClose;
		
		[Header("---- Item Stat ----")]
		[SerializeField] private Transform statParent;

		[Header("---- Equip ----")] 
		[SerializeField] private Image farm_Image;
		[SerializeField] private Sprite default_farmIcon;
		[SerializeField] private Image action_Image;
		[SerializeField] private Sprite default_actionIcon;

		[Header("---- Player Stat Slider ----")] 
		[SerializeField] private Slider hpSlider_UI;
		[SerializeField] private Slider attackSlider_UI;
		[SerializeField] private Slider defenseSlider_UI;
		[SerializeField] private Slider farmSlider_UI;

		[Header("---- Button ----")] 
		[SerializeField] private Button equipButton;

		[Header("---- NFT Sale ----")] 
		[SerializeField] private Button saleAsNft_Button;
		[SerializeField] private Button close_SaleAsNft_Button;

		[HideInInspector] public SOPlayerStat playerStatSO = null;


		private int _whichIndexSelected;
		public int WhichIndex
		{
			get => _whichIndexSelected;
			set => _whichIndexSelected = value;
		}
		
		public string _whichTokenJson;
		public string WhichTonenJsonName
		{
			get => _whichTokenJson;
			set => _whichTokenJson = value;
		}
		
		

		private const string statPanel = "Item Detail";
		private const string nftSalePanel = "NFT Sale";
		
		
		
		
		
		
		
		
		
		#endregion

		#region Function
		//--------------------------------------------------------------------------------------------------------------
		private void Start()
		{
			
			WhichIndex = -1;
			itemDetailPanelClose.onClick.RemoveAllListeners();
			itemDetailPanelClose.onClick.AddListener(() =>
			{
				CloseWeaponStat();
			});
			
			equipButton.onClick.RemoveAllListeners();
			equipButton.onClick.AddListener(() =>
			{
			var heroinventory  = inventory.Container[WhichIndex].item;
			switch (heroinventory.inventoryItemType)
			{
				case InventoryItemType.ActionWeapon:
					playerStatSO.actionWeapon_Icon = heroinventory.itemIcon;
					playerStatSO.attack = heroinventory.weaponAttack;
					playerStatSO.defense = heroinventory.weaponDefense;
					playerStatSO.actionTokenId = heroinventory.tokenId;
					playerStatSO.actionJsonUpdateFileName =
						heroinventory.name + heroinventory.description + "_" + heroinventory.tokenId;
					break;
				case InventoryItemType.Farmweapon:
					playerStatSO.farmWeapon_Icon = heroinventory.itemIcon;
					playerStatSO.farmPower = heroinventory.weaponFarm;
					playerStatSO.farmTokenId = heroinventory.tokenId;
					playerStatSO.farmJsonUpdateFileName =
						heroinventory.name + heroinventory.description + "_" + heroinventory.tokenId;
					
					break;
				case InventoryItemType.Shield:
					break;
				case InventoryItemType.Hero:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			SO_DATAController.singleton.UpdatePlayerEquipedItem();
				
				
			});
			
			
			// NFT SALE
			
			saleAsNft_Button.onClick.RemoveAllListeners();
			saleAsNft_Button.onClick.AddListener(() =>
			{
				ShowMenu(nftSalePanel);
			});
			
			close_SaleAsNft_Button.onClick.RemoveAllListeners();
			close_SaleAsNft_Button.onClick.AddListener(() =>
			{
				CloseMenu(nftSalePanel);
			});
			
			
			
		}


		//--------------------------------------------------------------------------------------------------------------
		private void OnEnable()
		{

			if (SO_DATAController.singleton != null && SO_DATAController.singleton.equipmentContainer != null)
			{
				SO_DATAController.singleton.DataSetHeroEquipmentContainer();

				StartCoroutine(SO_DATAController.singleton.FetchTokenDataToContainer());
				
				inventory = SO_DATAController.singleton.equipmentContainer;
			}

			if (playerStatSO == null)
			{
				playerStatSO = Resources.Load<SOPlayerStat>("Player/so_playerStat");
			}
			
			

			CreateSlotItem();
		}

		private void OnDisable()
		{
			
		}

		//--------------------------------------------------------------------------------------------------------------

		private void Update()
		{
			UpdateDisplayItem();
			EquipIconStatus();
			EquipPlayerStat();
		}
		
		//--------------------------------------------------------------------------------------------------------------

		void EquipIconStatus()
		{
			if (playerStatSO.actionWeapon_Icon == null)
			{
				action_Image.sprite = default_actionIcon;
			}
			else
			{
				action_Image.sprite = playerStatSO.actionWeapon_Icon;
			}
			
			
			if (playerStatSO.farmWeapon_Icon == null)
			{
				farm_Image.sprite = default_farmIcon;
			}
			else
			{
				farm_Image.sprite = playerStatSO.farmWeapon_Icon;
			}
		}


		void EquipPlayerStat()
		{
			if (hpSlider_UI != null)
			{
				hpSlider_UI.value = playerStatSO.hp;
			}
			
			if (attackSlider_UI != null)
			{
				attackSlider_UI.value = playerStatSO.attack;
			}
			
			if (defenseSlider_UI != null)
			{
				defenseSlider_UI.value = playerStatSO.defense;
			}
			
			if (farmSlider_UI != null)
			{
				farmSlider_UI.value = playerStatSO.farmPower;
			}
		}

		//--------------------------------------------------------------------------------------------------------------

		void CreateSlotItem()
		{
			if (inventory.Container.Count == 0)
			{
				itemsDisplayed.Clear();
			}
			for (int i = 0; i < inventory.Container.Count; i++)
			{
				if (itemsDisplayed.ContainsKey(inventory.Container[i])) return;
				if (inventory.Container[i].item.slotPrefabUI == null) return;
				
				GameObject slotPrefab = Instantiate(inventory.Container[i].item.slotPrefabUI) as GameObject;
				slotPrefab.transform.SetParent(slotParent, false);
				
				slotPrefab.GetComponentInChildren<TextMeshProUGUI>().text = "Lv." + inventory.Container[i].item.weaponLevel.ToString();
				slotPrefab.GetComponent<UI_EquipmentSelection>().slotIcon.sprite = inventory.Container[i].item.itemIcon;
				slotPrefab.GetComponent<UI_EquipmentSelection>().itemID = inventory.Container[i].item.ID;
				slotPrefab.GetComponent<UI_EquipmentSelection>().indexOfData = i;
				itemsDisplayed.Add(inventory.Container[i], slotPrefab);
			}
		}
		
		//--------------------------------------------------------------------------------------------------------------

		void UpdateDisplayItem()
		{
			if (inventory.Container.Count == 0) return;
			for (int i = 0; i < inventory.Container.Count; i++)
			{
				if (itemsDisplayed.ContainsKey(inventory.Container[i]))
				{
					if (itemsDisplayed[inventory.Container[i]] != null)
					{
						itemsDisplayed[inventory.Container[i]].GetComponentInChildren<TextMeshProUGUI>().text = "Lv." + inventory.Container[i].item.weaponLevel.ToString();
					}
					
				}
				else
				{
					GameObject slotPrefab = Instantiate(inventory.Container[i].item.slotPrefabUI) as GameObject;
					slotPrefab.transform.SetParent(slotParent, false);
				
					slotPrefab.GetComponentInChildren<TextMeshProUGUI>().text = "Lv." + inventory.Container[i].item.weaponLevel.ToString();
					slotPrefab.GetComponent<UI_EquipmentSelection>().slotIcon.sprite = inventory.Container[i].item.itemIcon;
					slotPrefab.GetComponent<UI_EquipmentSelection>().itemID = inventory.Container[i].item.ID;
					slotPrefab.GetComponent<UI_EquipmentSelection>().indexOfData = i;
					itemsDisplayed.Add(inventory.Container[i], slotPrefab);
				}
			}
		}
		
		
		//--------------------------------------------------------------------------------------------------------------
		
		public void RemoveFromContainer(int _id)
		{
			for (int i = 0; i < inventory.Container.Count; i++)
			{
				if (inventory.Container[i].item.ID == _id)
				{
					inventory.Container.RemoveAt(i);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------------------------
		public void AddItemDataUI(int _rank, string _itemNameText, string _infoText, Sprite _itemIcon, List<GameObject> prefab)
		{
			itemRankText.text = _rank.ToString();
			itemNameText.text = _itemNameText.ToString();
			infoText.text = _infoText.ToString();
			itemIcon.sprite = _itemIcon;
			WhichTonenJsonName = _itemNameText + _infoText;
			foreach (var obj in prefab)
			{
				GameObject go = Instantiate(obj) as GameObject;
				go.transform.SetParent(statParent, false);
			}
		}
		
		
		//--------------------------------------------------------------------------------------------------------------
		
		public void ResetDataByDefault()
		{
			itemNameText.text = String.Empty;
			infoText.text = String.Empty;
			if (statParent.childCount != 0)
			{
				for (int i = 0; i < statParent.childCount; i++)
				{
					Destroy(statParent.GetChild(i).gameObject);
				}
			}
			
		}
		
		//--------------------------------------------------------------------------------------------------------------

		public void OpenWeaponStat()
		{
			ShowMenu(statPanel);
		}

		public void CloseWeaponStat()
		{
			CloseMenu(statPanel);
			UI_EquipmentSelection.singleton.Deselect();
			ResetDataByDefault();
		}
		
		#endregion
		
		
		//--------------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------------
		public SO_HeroEquipmentSlotContainer GetContainer() => inventory;
		
    }
}//Namespace


