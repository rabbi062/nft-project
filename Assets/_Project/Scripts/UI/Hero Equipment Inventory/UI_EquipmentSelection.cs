using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TD
{
    public class UI_EquipmentSelection : MonoBehaviour, IPointerClickHandler
    {
	    
	    #region Singleton

	    public static UI_EquipmentSelection singleton;

	    private void Awake()
	    {
		    if (singleton == null)
		    {
			    singleton = this;
		    }
	    }

	    #endregion
	    
	    
		#region Variable

		[Header("---- Border ----")] 
		[SerializeField] private GameObject Border;
	    public Image slotIcon;
		
		
		[HideInInspector] public int itemID;
		[HideInInspector] public int indexOfData;
		
		List<GameObject> prefab = new List<GameObject>();
	
		#endregion

		#region Function
		
		//--------------------------------------------------------------------------------------------------------------
		private void Start()
		{
			if (Border == null) return;
			Border.SetActive(false);
			
		}


		//--------------------------------------------------------------------------------------------------------------
		public void OnPointerClick(PointerEventData eventData){
			OnClick();
			if (UI_HeroEquipmentInventoryManager.singleton != null)
			{
				UI_HeroEquipmentInventoryManager.singleton.WhichIndex = indexOfData;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		public void Deselect()
		{
			ResetAllButton();
			if (UI_HeroEquipmentInventoryManager.singleton != null)
			{
				UI_HeroEquipmentInventoryManager.singleton.WhichIndex = -1;
			}
		}
		
		
		//--------------------------------------------------------------------------------------------------------------
		public void OnClick()
		{
			ResetAllButton();
			
			if (Border == null) return;
			Border.SetActive(true);

			if (UI_HeroEquipmentInventoryManager.singleton != null)
			{
				UI_HeroEquipmentInventoryManager.singleton.ResetDataByDefault();
				prefab.Clear();

				SO_WeaponItem data = UI_HeroEquipmentInventoryManager.singleton.GetContainer()
					.Container[GetIndexEquipmentSelection()].item;
				
				// Add Data
				switch (data.inventoryItemType)
				{
					case InventoryItemType.ActionWeapon:
						if (data.attackPrefab_UI == null && data.defensePrefab_UI == null) return;

						data.attackPrefab_UI.transform.GetChild(1).GetComponent<TMP_Text>().text =
							"+" + data.weaponAttack;
						prefab.Add(data.attackPrefab_UI);
						
						data.defensePrefab_UI.transform.GetChild(1).GetComponent<TMP_Text>().text =
							"+" + data.weaponDefense;
						prefab.Add(data.defensePrefab_UI);
						
						
						UI_HeroEquipmentInventoryManager.singleton.AddItemDataUI(data.weaponLevel, data.name, data.description, data.itemIcon, prefab);
						
						break;
					case InventoryItemType.Farmweapon:
						if (data.farmPrefab_UI == null ) return;
						
						data.farmPrefab_UI.transform.GetChild(1).GetComponent<TMP_Text>().text =
							"+" + data.weaponFarm;
						prefab.Add(data.farmPrefab_UI);
						
						UI_HeroEquipmentInventoryManager.singleton.AddItemDataUI(data.weaponLevel, data.name, data.description, data.itemIcon, prefab);
						break;
					case InventoryItemType.Shield:
						break;
					case InventoryItemType.Hero:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				
				
				
				
				
				
				
				
				
				UI_HeroEquipmentInventoryManager.singleton.OpenWeaponStat();
			}
		}
		
		
		
		
		//--------------------------------------------------------------------------------------------------------------
		public void ResetAllButton()
		{
			UI_EquipmentSelection[] allButtons = GameObject.FindObjectsOfType<UI_EquipmentSelection>();
			foreach (UI_EquipmentSelection button in allButtons)
			{
				button.Border.SetActive(false);
			}
		}
		
		
		
		
		//--------------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------------
		public int GetIndexEquipmentSelection() => indexOfData;
		public int GetItemID() => itemID;

		#endregion

    }
    
}


