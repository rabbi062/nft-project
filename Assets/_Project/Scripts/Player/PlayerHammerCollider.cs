using System;
using UnityEngine;

namespace TD
{
    public class PlayerHammerCollider : MonoBehaviour
    {
	#region Variable
	
	
	
	
	
	
	
	
	
	
	
	
	//------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------
	public static PlayerHammerCollider Instance
	{
		get { return CsSingleton<PlayerHammerCollider>.GetInstance(); }
	}
	
	#endregion

	#region Function
	
	
	
	#endregion
	
	
	#region Collision Callbacks
	

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("GrassTile"))
		{
			if (PlayerCharacter.Instance.playerStatSO != null)
			{
				FloorTileBehaviour.Instance.FloorTileHitByHammer(other.gameObject, PlayerCharacter.Instance.playerStatSO.farmPower);
			}
			
		}
	}

	#endregion
	
	
	
	
	
    }
}


