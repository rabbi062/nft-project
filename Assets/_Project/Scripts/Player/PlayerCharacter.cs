using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace TD
{
    public class PlayerCharacter : MonoBehaviour
    {
	#region Variable

	
	[SerializeField] private float moveSpeed;
	[SerializeField] private Transform weaponParent;

	//------------------------------------------------------------------------------------------------------------------
	private Rigidbody rb;
	private FixedJoystick joystick;
	public bool move;


	public Animator playerAnim;
	
	[HideInInspector]public SOPlayerStat playerStatSO = null;
	//------------------------------------------------------------------------------------------------------------------

	public static PlayerCharacter Instance;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	#endregion

	#region Function
	//------------------------------------------------------------------------------------------------------------------
	private void Start()
	{
		if (playerStatSO == null)
		{
			playerStatSO = Resources.Load<SOPlayerStat>("Player/so_playerStat");
		}
		
		
		if (playerAnim == null)
		{
			playerAnim = GetComponentInChildren<Animator>();
		}
		
		
		if (rb == null)
		{
			rb = Object.FindObjectOfType<PlayerCharacter>().GetComponent<Rigidbody>();
		}
		
		if (joystick == null)
		{
			joystick = Object.FindObjectOfType<FixedJoystick>();
		}
	}

	private void Update()
	{
		if (joystick.Horizontal != 0 || joystick.Vertical != 0)
		{
			move = true;
			Move(Time.deltaTime);
		}
		else
		{
			move = false;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void Move(float deltaTime)
	{
		Vector3 movement = new Vector3(-1*joystick.Horizontal, 0, -1*joystick.Vertical);

		movement *= (moveSpeed * deltaTime);
		if (rb != null)
		{
			rb.velocity = movement;
		}
	}



	public void Attack()
	{
		if (playerStatSO == null) return;

		if (playerStatSO.actionWeapon_Object != null)
		{
			GameObject w = Instantiate(playerStatSO.actionWeapon_Object);
			w.transform.SetParent(weaponParent, false);
			this.Wait(1.2f, () =>
			{
				Destroy(w);
			});
		}
		
		//
		
		playerAnim.SetTrigger("Attack");
	}

	public void Farm()
	{
		if (playerStatSO == null) return;

		if (playerStatSO.farmWeapon_Object != null)
		{
			GameObject w = Instantiate(playerStatSO.farmWeapon_Object);
			w.transform.SetParent(weaponParent, false);
			this.Wait(1.2f, () =>
			{
				Destroy(w);
			});
		}
		
		//
		
		playerAnim.SetTrigger("Dig");
	}
	

	#endregion

    }
}


