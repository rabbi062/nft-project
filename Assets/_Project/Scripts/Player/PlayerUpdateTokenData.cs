using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.Engine;
using UnityEngine;
using UnityEngine.Events;

namespace TD
{
    public class PlayerUpdateTokenData : MonoBehaviour
    {
	    #region Singleton

	    public static PlayerUpdateTokenData singleton;

	    private void Awake()
	    {
		    if (singleton == null)
		    {
			    singleton = this;
		    }
		    else
		    {
			    Destroy(this);
		    }
	    }

	    #endregion

	    #region Variable

	    [HideInInspector] public SO_HeroEquipmentSlotContainer playerEquipedTokenContainer;
	    

	    #endregion

	    #region Function

	    private void Start()
	    {
		    if (playerEquipedTokenContainer == null )
		    {
			    playerEquipedTokenContainer = Resources.Load<SO_HeroEquipmentSlotContainer>("Hero Equiped Weapon Update");
		    }
	    }

	    public IEnumerator SaveUpdatedToken(Token token, UnityAction<string> callback)
	    {
		    string saveData = JsonUtility.ToJson(token, true);
		    BinaryFormatter bf = new BinaryFormatter();
		    string filename = token.SystemName + token.Description + "_" + token.Id;
		    
		    FileStream file = File.Create(Application.persistentDataPath  + GlobalName.tokenPath + filename);
		    bf.Serialize(file, saveData);
		    file.Close();

		    yield return null;
		    callback?.Invoke("SUCCESS");
	    }

	    public IEnumerator SaveUpdatedTokenData(string _filename, UnityAction<string> callback)
	    {
		    var loadedToken = ScriptableObject.CreateInstance<Token>();
		    StartCoroutine(LoadUpdatedToken(_filename, t =>
		    {
			    loadedToken = t;
		    }));

		    yield return null;
		    callback?.Invoke("SUCCESS");
	    }


	    public void AddEquipedItemToContainer(string filename)
	    {
		    if (!File.Exists(Application.persistentDataPath + GlobalName.tokenPath + filename))
		    {
			    return;
		    }
		    
		    
		    var loadedToken = ScriptableObject.CreateInstance<Token>();
		    StartCoroutine(LoadUpdatedToken(filename, t =>
		    {
			    loadedToken = t;
			    
			    
			    var itemSO = ScriptableObject.CreateInstance<SO_WeaponItem>();
			    itemSO.tokenId = loadedToken.Id;
			    itemSO.name = loadedToken.SystemName;
			    itemSO.description = loadedToken.Description;
			    var a = loadedToken.GetTokenDefinition();
			    foreach (var attr in a.attributes)
			    {
				    switch (attr.name)
				    {
					    case "Attack Damage":
						    itemSO.weaponAttack = int.Parse(attr.value);
						    break;
					    case "Defense":
						    itemSO.weaponDefense = int.Parse(attr.value);
						    break;
					    case "Farm Power":
						    itemSO.weaponFarm = int.Parse(attr.value);
						    break;
					    case "Level":
						    itemSO.weaponLevel = int.Parse(attr.value);
						    break;
					    case "Price":
        							
						    break;
				    }
			    }
			    
			    if (itemSO.name == "Axe" || itemSO.name == "Sword")
			    {
				    itemSO.inventoryItemType = InventoryItemType.ActionWeapon;
			    }
			    if(itemSO.name == "Hammer" || itemSO.name == "Blade")
			    {
				    itemSO.inventoryItemType = InventoryItemType.Farmweapon;
			    }

			    playerEquipedTokenContainer.AddItemWeapon(itemSO);
		    }));
		    
		    
	    }


	    public void RemoveEquipedItemToContainer()
	    {
		    playerEquipedTokenContainer.Container.Clear();
	    }

	    public IEnumerator LoadUpdatedToken(string filename, UnityAction<Token> result)
	    {
		    var tokenData = ScriptableObject.CreateInstance<Token>();
		    
		    BinaryFormatter bf = new BinaryFormatter();
		    if (File.Exists(Application.persistentDataPath  + GlobalName.tokenPath + filename))
		    {
			    FileStream file = File.Open(Application.persistentDataPath  + GlobalName.tokenPath + filename, FileMode.Open);
        
			    JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), tokenData);
			    file.Close();
		    }
		    
		    yield return null;
			result?.Invoke(tokenData);
	    }


	    public bool IsUpdateTokenFileExist(string filename)
	    {
		    bool b = File.Exists(Application.persistentDataPath + GlobalName.tokenPath + filename);
		    return b;
	    }
	    

	    #endregion

    }
}


