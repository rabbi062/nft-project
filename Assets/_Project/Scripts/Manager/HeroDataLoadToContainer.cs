using System;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.Engine;
using HeathenEngineering.BGSDK.Examples;

namespace TD
{
    public class HeroDataLoadToContainer : MonoBehaviour
    {
	#region Variable
	
	[HideInInspector] public SO_HeroContainer heroContainer;
	
	#endregion

	#region Function

	private void Start()
	{
		if (heroContainer == null)
		{
			heroContainer = Resources.Load<SO_HeroContainer>("Hero Container");
		}
		
		heroContainer.HeroContainer.Clear();
		StartCoroutine(AddHeroDataListToServer.FetchHeroListData(delegate(List<AllHeroNftName> result)
		{
			if (result.Count > 0)
			{
				for (int i = 0; i < result.Count; i++)
				{
					try
					{
						if (File.Exists(Application.persistentDataPath + GlobalName.tokenPath + result[i].name))
						{
							heroContainer.AddItemHero(LoadHeroDataToContainer(result[i].name, i));
						}
							

					}
					catch (Exception e)
					{
						Console.WriteLine(e);
						throw;
					}
				}
			}
					
					
		}));
	}

	private void OnEnable()
	{
		
	}

	//--------------------------------------------------------------------------------------------------------------

	private SO_Hero LoadHeroDataToContainer(string serverHeroName , int i)
	{
		var tokenSO = ScriptableObject.CreateInstance<Token>();
        		
		BinaryFormatter bf = new BinaryFormatter();
		if (File.Exists(Application.persistentDataPath  + GlobalName.tokenPath + serverHeroName))
		{
			FileStream file = File.Open(Application.persistentDataPath  + GlobalName.tokenPath + serverHeroName, FileMode.Open);
        
			JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), tokenSO);
			file.Close();
		}
			
		var itemSO = ScriptableObject.CreateInstance<SO_Hero>();
		itemSO.slotPrefabUI = DataGateWay.singleton.heroPrefab_UI;
		itemSO.tokenId = tokenSO.Id;
		itemSO.name = tokenSO.SystemName;
		itemSO.description = tokenSO.Description;
			
		var a = tokenSO.GetTokenDefinition();
		foreach (var attr in a.attributes)
		{
			switch (attr.name)
			{
				case "HP":
					itemSO.heroHp = int.Parse(attr.value);
					break;
				case "Level":
					itemSO.heroLevel = int.Parse(attr.value);
					break;
				case "Type":
					if (attr.value == "Hero")
					{
						itemSO.inventoryItemType = InventoryItemType.Hero;
					}
					break;
			}
		}
			
			
			
		itemSO.itemIcon = DataGateWay.singleton.heroSprite[i];
			
			
		return itemSO;
	}
	

	#endregion
        
    }
}


