using System;
using HeathenEngineering.BGSDK.Engine;
using UnityEngine;
using UnityEngine.UI;

namespace HeathenEngineering.BGSDK.API
{
    public class NFTExtraManager : MonoBehaviour
    {
	#region Variable
	[Header("---- Deploy Contract ----")] 
	[SerializeField] private string contractName;
	[TextArea(10,10)][SerializeField] private string description;
	[SerializeField] private string imageUrl;
	[SerializeField] private string externalUrl;
	[SerializeField] private string chain;
	[SerializeField] private string owner;
	[SerializeField] private Button deployContract_Button;

	[Header("---- Mint NFT ----")] 
	[SerializeField] private Contract contract;
	[SerializeField] private string[] address;
	[SerializeField] private int tokenId;
	
	

	#endregion

	#region Function

	private void Start()
	{
		
		// deployContract_Button.onClick.RemoveAllListeners();
		// deployContract_Button.onClick.AddListener(() =>
		// {
		// 	if (!string.IsNullOrEmpty(contractName) && !string.IsNullOrEmpty(description) && !string.IsNullOrEmpty(chain) && !string.IsNullOrEmpty(owner)) return;
		// 	
		// 	StartCoroutine(API.Server.Nft.DeployNftContract(contractName, description, imageUrl, externalUrl,chain,owner,
		// 		result =>
		// 		{
		// 			Debug.Log(result.message);
		// 		}));
		// });
		this.Wait(5f, () =>
		{
			//MintNftTest();
		});
		
	}



	void MintNftTest()
	{
		// StartCoroutine(API.Server.TokenMint.MintNonFungibleToken(tokenId, contract, address, result =>
		// {
		// 	Debug.Log(result.message);
		// }));
	}
	
	

	#endregion

    }
}


