using MoreMountains.Tools;
using UnityEngine;

namespace TD
{
    public class LevelManager : MonoBehaviour
    {
	    #region Singleton
        
	    public static LevelManager singleton;

	    private void Awake()
	    {
		    if (singleton == null)
		    {
			    singleton = this;
		    }
		    else
		    {
			    Destroy(this.gameObject);
		    }
	    }
	    #endregion
	    
	    
	#region Variable
	
	#endregion

	#region Function
	
	public void LoadLevel(string scene)
	{
		LoadingSceneManager.LoadScene(scene);
	}
	
	#endregion
        
    }
}


