using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.API;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UI.Pagination;
using TD.Utilities;
using UnityEngine.UI;


namespace TD
{
	public class NFTShop : MonoBehaviour
	{
		#region Singleton
		public static NFTShop singleton;

		private void Awake()
		{
			if (singleton == null) singleton = this;
		}


		#endregion

		#region Variable

		[SerializeField] private GameObject shopPrefab;
		[SerializeField] private Transform shopItemParent; //pagination : content
		[Header("--- Pages ---")]
		[SerializeField] PagedRect pagedRect;
		int numberPages = 10;

		[Header("--- Object Pool ---")]
		GameObject[] objectPool;


		public List<Offer> offerData;


		/// <summary>
		/// Buy Nft From Store
		/// </summary>
		private string _walletAddress;
		public string Wallet_Address
		{
			get => _walletAddress;
			set => _walletAddress = value;
		}

		private string _externalUserId = "rabbi.zaynax@gmail.com";
		public string External_UserId
		{
			get => _externalUserId;
			set => _externalUserId = value;
		}


		private string _offerId;
		public string Offer_Id
		{
			get => _offerId;
			set => _offerId = value;
		}


		private int _offerPrice;

		public int Offer_Price
		{
			get => _offerPrice;
			set => _offerPrice = value;
		}

		private string _offerName;

		public string Offer_Name
		{
			get => _offerName;
			set => _offerName = value;
		}


		#endregion

		#region Function

		private void Start()
		{
			InitPagination();
			Debug.Log(Application.persistentDataPath);
			this.Wait(3f, () =>
			{
				StartCoroutine(Server.Market.GetOfferListByContractAddress(HandleListOffer));
			});

		}


		//--------------------------------------------------------------------------------------------------------------
		private void HandleListOffer(ListOfOfferResult listOfferResult)
		{
			offerData = listOfferResult.result;
			//ProcessOfferData(offerData);
			List<OfferList> result = new List<OfferList>();
			for (int i = 0; i < offerData.Count; i++)
			{
				result.Add(new OfferList(i, offerData[0]));
			}


			SaveToJson<OfferList>(result);

			if (pagedRect.Pages.Count > 0)
			{
				GridLayoutGroup layout = pagedRect.Pages[0].GetComponentInChildren<GridLayoutGroup>(true);
				if (layout != null) layout.gameObject.SetActive(true);
				shopItemParent = layout.transform;
				ProcessOfferData(offerData, shopItemParent);
			}
		}

		void SaveToJson<T>(List<T> tosave)
		{
			string content = JsonHelper.ToJson<T>(tosave.ToArray());
			StartCoroutine(SaveListOfferDataToJson(content));
		}
		
		// Add data to server
		
		IEnumerator SaveListOfferDataToJson(string data)
		{
			if (!Directory.Exists(Application.persistentDataPath  + GlobalName.tokenPath))
			{
				Directory.CreateDirectory(Application.persistentDataPath  + GlobalName.tokenPath);
			}

			string saveData = data;
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(Application.persistentDataPath  + GlobalName.tokenPath + GlobalName.LIST_OFFER_JSON);
			bf.Serialize(file, saveData);
			file.Close();
			yield return null;
		}
		

		//--------------------------------------------------------------------------------------------------------------
		void ProcessOfferData(List<Offer> OfferDatas)
		{
			foreach (var data in OfferDatas)
			{
				GameObject sItem = Instantiate(shopPrefab);
				sItem.transform.SetParent(shopItemParent, false);
				sItem.transform.GetChild(1).GetComponent<TMP_Text>().text = data.nft.name.ToString();
				sItem.GetComponent<ShopItemInfo>().offerId = data.id;
				sItem.GetComponent<ShopItemInfo>().status = data.status;
				sItem.GetComponent<ShopItemInfo>().price = data.price;
				sItem.GetComponent<ShopItemInfo>().name = data.nft.name;
			}
		}

		void ProcessOfferData(List<Offer> OfferDatas, Transform newParent)
		{
			for (int i = 0; i < offerData.Count; i++)
			{
				Offer data = offerData[i];

				GameObject sItem = objectPool[i];// Instantiate(shopPrefab);
				sItem.transform.SetParent(newParent, false);
				sItem.transform.GetChild(1).GetComponent<TMP_Text>().text = data.nft.name.ToString();
				sItem.GetComponent<ShopItemInfo>().offerId = data.id;
				sItem.GetComponent<ShopItemInfo>().status = data.status;
				sItem.GetComponent<ShopItemInfo>().price = data.price;
				sItem.GetComponent<ShopItemInfo>().name = data.nft.name;
			}
		}

		//--------------------------------------------------------------------------------------------------------------

		public IEnumerator BuyNFtFromShop(UnityAction<string> successful)
		{
			Wallet_Address = PlayerPrefs.GetString("Wallet Address");  //PlayerPrefs.GetString("Wallet Address"); // TangerineNfts//0x65722C007e93bCf44a802529D3491B8B7C925A9e

			if (string.IsNullOrEmpty(Wallet_Address) || string.IsNullOrEmpty(External_UserId) || string.IsNullOrEmpty(_offerId))
			{
				yield return null;
			}

			StartCoroutine(Server.Market.BuyItemOffer(Wallet_Address, External_UserId, Offer_Id, result =>
			{

				successful.Invoke("SUCCESS");
				StartCoroutine(Server.Market.GetOfferListByContractAddress(HandleListOffer));

			}));


		}

		void InitPagination()
		{
			//init pages
			for (int i = 0; i < numberPages; i++)
			{
				Page page = pagedRect.AddPageUsingTemplate();
				page.PageNumber = i + 1;
				GridLayoutGroup layout = page.GetComponentInChildren<GridLayoutGroup>(true);
				page.PageTitle = "NFT Page Number " + page.PageNumber;
				layout.gameObject.SetActive(false);
			}

			pagedRect.UpdatePagination();

			//init object pool
			objectPool = new GameObject[100];
			for (int i = 0; i < objectPool.Length; i++)
			{
				objectPool[i] = Instantiate(shopPrefab);
				//objectPool[i].SetActive(false);
			}
		}


		public void OnPageChanged(Page current, Page last)
		{

			Debug.Log("On Page");
			Debug.Log(current.PageNumber);

			if (current.PageNumber != last.PageNumber)
			{
				GridLayoutGroup current_layout = current.GetComponentInChildren<GridLayoutGroup>(true);
				if (current_layout != null)
					current_layout.gameObject.SetActive(true);

				GridLayoutGroup last_layout = last.GetComponentInChildren<GridLayoutGroup>(true);
				if (last_layout != null)
					last_layout.gameObject.SetActive(false);

			}


			GridLayoutGroup layout = current.GetComponentInChildren<GridLayoutGroup>(true);
			shopItemParent = layout.transform;
			ProcessOfferData(offerData, shopItemParent);

		}



		#endregion

	}


	[Serializable]
	public class OfferList
	{
		public int id;
		public Offer data;
		public OfferList (int _id, Offer _data)
		{
			this.id = _id;
			this.data = _data;
		}
		
	}
	
	public static class JsonHelper
	{
		public static T[] FromJson<T>(string json)
		{
			Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
			return wrapper.result;
		}

		public static string ToJson<T>(T[] array)
		{
			Wrapper<T> wrapper = new Wrapper<T>();
			wrapper.result = array;
			return JsonUtility.ToJson(wrapper);
		}

		public static string ToJson<T>(T[] array, bool prettyPrint)
		{
			Wrapper<T> wrapper = new Wrapper<T>();
			wrapper.result = array;
			return JsonUtility.ToJson(wrapper, prettyPrint);
		}

		[Serializable]
		private class Wrapper<T>
		{
			public T[] result;
		}
	}
}