using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.DataModel;
using HeathenEngineering.BGSDK.Engine;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.Events;
using WalletConnectSharp.Unity;

namespace TD
{
    public static class UnlockWeapon 
    {
	#region Variable

	public static ServerNftData serverNftData;
	
	static List<AllUnlockableNftName> _allUnlockableNftName = new List<AllUnlockableNftName>();
	public static List<AllUnlockableNftName> listData = new List<AllUnlockableNftName>();
	public static List<AllUnlockableNftName> d_data  = new List<AllUnlockableNftName>();
	public static List<AllUnlockableNftName> update_data  = new List<AllUnlockableNftName>();
	public static string _name = null;
	#endregion

	#region Function

		//--------------------------------------------------------------------------------------------------------------
		public static IEnumerator GetUnlockableWeaponDataFromServer(string weaponTokenName, UnityAction<string> result)
		{
			
			if (PlayFabAuthService.PlayfabSuccessfulLogin)
			{
				
				
				PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(), 
				result =>
				{
					if (result.Data == null || !result.Data.ContainsKey(weaponTokenName))
					{
						return;
					}
					else
					{
						serverNftData = JsonUtility.FromJson<ServerNftData>(result.Data[weaponTokenName]);
						
						//

						var unlockableToken = ScriptableObject.CreateInstance<Token>();
						unlockableToken.SystemName = serverNftData.data.name;
						unlockableToken.Description = serverNftData.data.description;
						unlockableToken.Image = serverNftData.data.image;
						//Attribute data
                
						var h = unlockableToken.GetTokenDefinition();
						h.attributes = new TokenAttributes[serverNftData.data.attributes.Count];
						 for (int j = 0; j < serverNftData.data.attributes.Count; j++)
						 {
							 h.attributes[j].name = serverNftData.data.attributes[j].name;
						 	h.attributes[j].type = serverNftData.data.attributes[j].type;
						 	h.attributes[j].value = serverNftData.data.attributes[j].value;
						 	h.attributes[j].maxValue = serverNftData.data.attributes[j].maxValue;
						 }
						
						 
						string saveData = JsonUtility.ToJson(unlockableToken, true);
						BinaryFormatter bf = new BinaryFormatter();
						_name = unlockableToken.SystemName + unlockableToken.Description;
						PlayerPrefs.SetString("Unlockable Weapon Name", _name);
						FileStream file = File.Create(Application.persistentDataPath  + GlobalName.tokenPath + _name);
						bf.Serialize(file, saveData);
						file.Close();


						// //string sData = JsonUtility.ToJson(_dataAll, true);
						// BinaryFormatter bif = new BinaryFormatter();
						// FileStream m_file = null;
						// m_file = new FileStream(Application.persistentDataPath  + GlobalName.tokenPath + GlobalName.ALL_UNLOCKABLENFT_NAMELIST, FileMode.Create);
						// bif.Serialize(m_file, sData);
						// m_file.Close();

					}
					
				}, error =>
				{
					Debug.Log("Got error getting titleData:");
					Debug.Log(error.GenerateErrorReport());
				});
			}
			
			yield return null;
			
			result?.Invoke("SUCCESS");
			
		} 
		//--------------------------------------------------------------------------------------------------------------
		public static IEnumerator UpdateAllUnlockableNftNameList(string weaponTokenName)
		{
			#region All Unlockable Nft List In Server

			PlayFabClientAPI.GetUserData(new GetUserDataRequest() {
				PlayFabId = PlayerPrefsUtility.GetPlayerPrefsString(GlobalName.PLAYFAB_ID, ""),
				Keys = null
			}, result => 
			{
				if (result.Data == null || !result.Data.ContainsKey(GlobalName.ALL_UNLOCKABLENFT_NAMELIST))
				{
					SeveUnlockableNameList(weaponTokenName);
				}
				else
				{
					var fetchData = JsonHelper.FromJson<AllUnlockableNftName>(result.Data[GlobalName.ALL_UNLOCKABLENFT_NAMELIST].Value);
					if (fetchData.Length > 0)
					{
						_allUnlockableNftName.Clear();
						foreach (var nameD in fetchData)
						{
							if (nameD.name != weaponTokenName)
							{
								var d = new AllUnlockableNftName
								{
									name = nameD.name,
									isMinted = nameD.isMinted
								};
								_allUnlockableNftName.Add(d);
							}
						}
					}

					SeveUnlockableNameList(weaponTokenName);
				}

				

			}, (error) => {
				Debug.Log("Got error retrieving user data:");
				Debug.Log(error.GenerateErrorReport());
			});
						
						
						
						
			#endregion
			
			
			yield return null;
			
			
		}


		private static void SeveUnlockableNameList(string weaponTokenName)
		{
			
			
			if (_allUnlockableNftName.Count > 0)
			{
				for (int i = 0; i < _allUnlockableNftName.Count; i++)
				{
					if (_allUnlockableNftName[i].name != weaponTokenName)
					{
						var m_d = new AllUnlockableNftName
						{
							name = weaponTokenName,
							isMinted = "false"
						};
						_allUnlockableNftName.Add(m_d);
					}
				}
			}
			else
			{
				var m_d = new AllUnlockableNftName
				{
					name = weaponTokenName,
					isMinted = "false"
				};
				_allUnlockableNftName.Add(m_d);
			}
			
			
			
			string sData = JsonHelper.ToJson(_allUnlockableNftName.ToArray());
			
			PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
			{
				Data = new Dictionary<string, string>() 
				{
					{GlobalName.ALL_UNLOCKABLENFT_NAMELIST, sData},
				}
			}, result =>
			{
				Debug.Log("Set internal user data successful");
			}, error =>
			{
				Debug.Log("Got error updating internal user data:");
				Debug.Log(error.GenerateErrorReport());
			});
		}

		//--------------------------------------------------------------------------------------------------------------
		public static IEnumerator FetchUnlockableListData(UnityAction<List<AllUnlockableNftName>> d)
		{
			
			
			if (PlayFabAuthService.PlayfabSuccessfulLogin)
			{
				
				PlayFabClientAPI.GetUserData(new GetUserDataRequest() {
					PlayFabId = PlayerPrefsUtility.GetPlayerPrefsString(GlobalName.PLAYFAB_ID, ""),
					Keys = null
				}, result => 
				{
					if (result.Data == null || !result.Data.ContainsKey(GlobalName.ALL_UNLOCKABLENFT_NAMELIST))
					{
						
						return;
					}
					else
					{
						
						var fetchData = JsonHelper.FromJson<AllUnlockableNftName>(result.Data[GlobalName.ALL_UNLOCKABLENFT_NAMELIST].Value);
						
						if (fetchData.Length > 0)
						{
							listData = new List<AllUnlockableNftName>(fetchData.Length);
							for (int i = 0; i < fetchData.Length; i++)
							{
								var da = new AllUnlockableNftName
								{
									name = fetchData[i].name,
									isMinted = fetchData[i].isMinted
								};
								listData.Add(da);
								
							}
							
						}
						
					}
					d.Invoke(listData);
					

				}, (error) => {
					Debug.Log("Got error retrieving user data:");
					Debug.Log(error.GenerateErrorReport());
				});
			}


			yield return null;

		}

		public static IEnumerator MakeUnlockableWeaponDataMintedTrue(string weaponTokenName, UnityAction<string> result)
		{

			if (PlayFabAuthService.PlayfabSuccessfulLogin)
			{
				
				
				PlayFabClientAPI.GetUserData(new GetUserDataRequest() {
					PlayFabId = PlayerPrefsUtility.GetPlayerPrefsString(GlobalName.PLAYFAB_ID, ""),
					Keys = null
				}, result => 
				{
					if (result.Data == null || !result.Data.ContainsKey(GlobalName.ALL_UNLOCKABLENFT_NAMELIST))
					{
						return;
					}
					else
					{
						
						var fetchData = JsonHelper.FromJson<AllUnlockableNftName>(result.Data[GlobalName.ALL_UNLOCKABLENFT_NAMELIST].Value);
						
						if (fetchData.Length > 0)
						{
							
							for (int i = 0; i < fetchData.Length; i++)
							{
								var da = new AllUnlockableNftName
								{
									name = fetchData[i].name,
									isMinted = fetchData[i].isMinted
								};
								
								d_data.Add(da);
							}
							
						}
						
						
						if (d_data.Count > 0)
						{
							update_data.Clear();
							for (int i = 0; i < d_data.Count; i++)
							{
								if (d_data[i].name == weaponTokenName)
								{
									var m_d = new AllUnlockableNftName
									{
										name = d_data[i].name,
										isMinted = "true"
									};
									update_data.Add(m_d);
								}
								else
								{
									var m_d = new AllUnlockableNftName
									{
										name = d_data[i].name,
										isMinted = "false"
									};
									update_data.Add(m_d);
								}
							}
						}
						
						string sData = JsonHelper.ToJson(update_data.ToArray());

						PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
						{
							Data = new Dictionary<string, string>() 
							{
								{GlobalName.ALL_UNLOCKABLENFT_NAMELIST, sData},
							}
						}, result =>
						{
							Debug.Log("Set internal user data successful");
						}, error =>
						{
							Debug.Log("Got error updating internal user data:");
							Debug.Log(error.GenerateErrorReport());
						});
						
						
						
						
						
					}
					
					

				}, (error) => {
					Debug.Log("Got error retrieving user data:");
					Debug.Log(error.GenerateErrorReport());
				});
			}
			yield return null;
			
			result?.Invoke("SUCCESS");
		}
	
	
	#endregion
        
    }


    [Serializable]
    public class ServerNftData
    {
	    public Data data;
	    
	    [Serializable]
	    public class Attribute
	    {
		    public string type;
		    public string name;
		    public string value;
		    public ulong maxValue;
	    }
	    [Serializable]
	    public class Data
	    {
		    public string name;
		    public string description;
		    public string image;
		    public string backgroundColor;
		    public string externalUrl;
		    public int currentSupply;
		    public int maxSupply;
		    public bool fungible;
		    public bool burnable;
		    public List<Attribute> attributes;
		    public string contractAddress;
		    public string id;
		    public bool confirmed;
		    public string rawData;
	    }
    }
    
    [Serializable]
    public class AllUnlockableNftName
    {
	    public string name;
	    public string isMinted;
    }
}


