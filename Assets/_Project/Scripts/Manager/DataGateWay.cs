using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HeathenEngineering.BGSDK.Engine;
using HeathenEngineering.BGSDK.Examples;
using UnityEngine;
using UnityEngine.Networking;


namespace TD
{
    public class DataGateWay : MonoBehaviour
    {
	    
	    #region Singleton

	    public static DataGateWay singleton;

	    private void Awake()
	    {
		    if (singleton == null)
		    {
			    singleton = this;
		    }
		    else
		    {
			    Destroy(this);
		    }
	    }

	    #endregion
	    
	    
	#region Variable

	[Header("---- Hero Equipment ----")] 
	public GameObject heroPrefab_UI;
	public List<Sprite> heroSprite;

	[Header("---- Hero Equipment ----")] 
	public GameObject slotPrefab_Ui;
	public GameObject attackDamage_Ui;
	public GameObject defense_Ui;
	public GameObject farm_Ui;
	public List<Sprite> sprite;
	


	#endregion

	#region Function

	private void Start()
	{

	}



	
	
	
	
	
	#region Save Image
		

	public void downloadImage(string url, string pathToSaveImage)
	{
		StartCoroutine(_downloadImage(url,pathToSaveImage));
	}

	private IEnumerator _downloadImage(string url, string savePath)
	{
		using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
		{
			yield return uwr.SendWebRequest();

			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.LogError(uwr.error);
			}
			else
			{
				Debug.Log("Success");
				Texture myTexture = DownloadHandlerTexture.GetContent(uwr);
				byte[] results = uwr.downloadHandler.data;
				saveImage(savePath, results);

			}
		}
	}

	void saveImage(string path, byte[] imageBytes)
	{
		//Create Directory if it does not exist
		if (!Directory.Exists(Path.GetDirectoryName(path)))
		{
			Directory.CreateDirectory(Path.GetDirectoryName(path));
		}

		try
		{
			File.WriteAllBytes(path, imageBytes);
			Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
		}
		catch (Exception e)
		{
			Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
			Debug.LogWarning("Error: " + e.Message);
		}
	}

	#endregion
	
	
	#endregion

    }
}


