using System;
using UnityEngine;

namespace TD
{
    public static class WalletInfoManager
    {
		#region Variable

		[HideInInspector] public static SO_WalletInfo soWalletInfo;

		#endregion

		#region Function

		public static void Init()
		{
			if (soWalletInfo == null)
			{
				soWalletInfo = Resources.Load<SO_WalletInfo>("Wallet Data");
			}
		}

		public static void SetLoginType(WalletLoginType type)
		{
			if(soWalletInfo == null) return;
			soWalletInfo.walletLoginType = type;
		}
		public static WalletLoginType GetWalletLoginType() => soWalletInfo.walletLoginType;
		//--------------------------------------------------------------------------------------------------------------
		public static void SetVenlyWalletAddress(string address)
		{
			if(soWalletInfo == null) return;
			soWalletInfo.v_walletAddress = address;
		}
		public static string GetVenlyWalletAddress() => !string.IsNullOrEmpty(soWalletInfo.v_walletAddress) ? soWalletInfo.v_walletAddress : String.Empty;
		
		public static void SetVenlyWalletChain(string chain)
		{
			if(soWalletInfo == null) return;
			soWalletInfo.v_walletChain = chain;
		}
		public static string GetVenlyWalletChain() => !string.IsNullOrEmpty(soWalletInfo.v_walletChain) ? soWalletInfo.v_walletChain : String.Empty;
		
		
		public static void SetVenlyWalletId(string id)
		{
			if(soWalletInfo == null) return;
			soWalletInfo.v_walletId = id;
		}
		public static string GetVenlyWalletId() => !string.IsNullOrEmpty(soWalletInfo.v_walletId) ? soWalletInfo.v_walletId : String.Empty;
		
		//--------------------------------------------------------------------------------------------------------------
		
		public static void SetMetamaskWalletAddress(string address)
		{
			if(soWalletInfo == null) return;
			soWalletInfo.m_walletAddress = address;
		}
		public static string GetMetamaskWalletAddress() => !string.IsNullOrEmpty(soWalletInfo.m_walletAddress) ? soWalletInfo.m_walletAddress : String.Empty;
		
		public static void SetMetamaskWalletChain(string chain)
		{
			if(soWalletInfo == null) return;
			soWalletInfo.m_walletChain = chain;
		}
		public static string GetMetamaskWalletChain() => !string.IsNullOrEmpty(soWalletInfo.m_walletChain) ? soWalletInfo.m_walletChain : String.Empty;
		#endregion

    }
}


