using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.Events;

namespace TD
{
    public static class AddHeroDataListToServer 
    {
	#region Variable
	
	private static List<AllHeroNftName> _allHeroNftName = new List<AllHeroNftName>();
	public static List<AllHeroNftName> fetchHeroName = new List<AllHeroNftName>();
	
	#endregion

	#region Function

	//------------------------------------------------------------------------------------------------------------------
	public static IEnumerator UpdateAllHeroNameList(string heroName)
	{
		if (PlayFabAuthService.PlayfabSuccessfulLogin)
		{
			PlayFabClientAPI.GetUserData(new GetUserDataRequest() {
				PlayFabId = PlayerPrefsUtility.GetPlayerPrefsString(GlobalName.PLAYFAB_ID, ""),
				Keys = null
			}, result => 
			{
				if (result.Data == null || !result.Data.ContainsKey(GlobalName.ALL_HERO_NAMELIST))
				{
					SeveUnlockableNameList(heroName);
				}
				else
				{
					var fetchData = JsonHelper.FromJson<AllUnlockableNftName>(result.Data[GlobalName.ALL_HERO_NAMELIST].Value);
					if (fetchData.Length > 0)
					{
						_allHeroNftName.Clear();
						foreach (var nameD in fetchData)
						{
							if (nameD.name != heroName)
							{
								var d = new AllHeroNftName
								{
									name = nameD.name,
								};
								_allHeroNftName.Add(d);
							}
						}
						SeveUnlockableNameList(heroName);
					}
			
					
				}
			
				
			
			}, (error) => {
				Debug.Log("Got error retrieving user data:");
				Debug.Log(error.GenerateErrorReport());
			});
		}
		yield return null;
	}

	private static void SeveUnlockableNameList(string heroName)
	{
		if (_allHeroNftName.Count > 0)
		{
			for (int i = 0; i < _allHeroNftName.Count; i++)
			{
				if (_allHeroNftName[i].name != heroName)
				{
					var m_d = new AllHeroNftName
					{
						name = heroName
					};
					_allHeroNftName.Add(m_d);
				}
			}
		}
		else
		{
			var m_d = new AllHeroNftName
			{
				name = heroName
			};
			_allHeroNftName.Add(m_d);
		}
		
		string sData = JsonHelper.ToJson(_allHeroNftName.ToArray());
			
		PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
		{
			Data = new Dictionary<string, string>() 
			{
				{GlobalName.ALL_HERO_NAMELIST, sData},
			}
		}, result =>
		{
			Debug.Log("Set internal user data successful");
		}, error =>
		{
			Debug.Log("Got error updating internal user data:");
			Debug.Log(error.GenerateErrorReport());
		});
	}
	
	//------------------------------------------------------------------------------------------------------------------

	public static IEnumerator FetchHeroListData(UnityAction<List<AllHeroNftName>> d)
	{
		if (PlayFabAuthService.PlayfabSuccessfulLogin)
		{
				
			PlayFabClientAPI.GetUserData(new GetUserDataRequest() {
				PlayFabId = PlayerPrefsUtility.GetPlayerPrefsString(GlobalName.PLAYFAB_ID, ""),
				Keys = null
			}, result => 
			{
				if (result.Data == null || !result.Data.ContainsKey(GlobalName.ALL_HERO_NAMELIST))
				{
						
					return;
				}
				else
				{
						
					var fetchData = JsonHelper.FromJson<AllHeroNftName>(result.Data[GlobalName.ALL_HERO_NAMELIST].Value);
						
					if (fetchData.Length > 0)
					{
						fetchHeroName = new List<AllHeroNftName>(fetchData.Length);
						for (int i = 0; i < fetchData.Length; i++)
						{
							var da = new AllHeroNftName
							{
								name = fetchData[i].name,
							};
							fetchHeroName.Add(da);
						}
							
					}
						
				}
				d.Invoke(fetchHeroName);
					

			}, (error) => {
				Debug.Log("Got error retrieving user data:");
				Debug.Log(error.GenerateErrorReport());
			});
		}

		yield return null;
	}
	
	#endregion
        
    }
    
    
    [Serializable]
    public class AllHeroNftName
    {
	    public string name;
    }
}


