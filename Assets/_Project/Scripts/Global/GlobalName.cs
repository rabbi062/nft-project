using UnityEngine;

namespace TD
{
    public static class GlobalName
    {

        public const string playerUsername = "Player Username";

        public const string LIST_OFFER_JSON = "List Offer Server";
        
        
        public const string tokenPath = "/Data/Token/";
        public const string allNFTFileName = "All Nft Name List";
        public const string ALL_UNLOCKABLENFT_NAMELIST = "All Unlockable Nft Name List";
        public const string ALL_HERO_NAMELIST = "All Hero Name List";
        
        //Wallet Info
        public const string currentUserWalletAddress = "Wallet Address";
        public const string currentUserWalletId = "Wallet ID";
        public const string currentUserWalletChain = "Wallet Chain";

        public const string PLAYFAB_ID = "Playfab Id";
        //Test
        public const string FIRSTTIME_LEVEL5UNLOCKWEAPON = "Level 5 Unlock Weapon"; // Playerprefs
        
        
        
        // Hero
        public const string HERO_SELECTION_INDEX = "Hero Selection Index";




    }
}


