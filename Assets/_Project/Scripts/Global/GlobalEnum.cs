using UnityEngine;

namespace TD
{
    public enum FloorTileType
    {
        GrassTile,
        SandTile,
        RockTile,
        LavaTile,
        SnowTile,
        NonDestructable
    }


}


