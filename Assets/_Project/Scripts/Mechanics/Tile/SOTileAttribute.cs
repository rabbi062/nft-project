using DG.Tweening;
using UnityEngine;

namespace TD
{
	[CreateAssetMenu(menuName = "Tile/TileAttribute", fileName = "tile attribute")]
    public class SOTileAttribute : ScriptableObject
    {
	    
	    //--------------------------------------------------------------------------------------------------------------
	    [Header("---- Setting ----")]
	    public FloorTileType floorTileType;
	    public int currentTileHealth;
	    public int maxTileHealth;
	    public bool isTileVisible = true;

	    [Header("---- Enemy ----")]
	    public bool isEnemySpawn;
    }
}


