using System;
using DG.Tweening;
using UnityEngine;
using UnityEditor;

namespace TD
{
	[RequireComponent(typeof(BoxCollider))]
    public class TileBaseSc : MonoBehaviour
    {
	#region Variable
	//------------------------------------------------------------------------------------------------------------------
	public SOTileAttribute soTileAttribute;

	//------------------------------------------------------------------------------------------------------------------
	private float effectDuration = 0.3f;
	private float shakeStrength = 0.1f;
	private int shakeVibrato = 3;
	private float shakeRandomness = 1;

	private Sequence sequence;

	private AudioClip tileHitClip;

	//------------------------------------------------------------------------------------------------------------------
	public static TileBaseSc Instance
	{
		get { return CsSingleton<TileBaseSc>.GetInstance(); }
	}
	#endregion

	#region Function

	private void Start()
	{
		if (!soTileAttribute.isTileVisible)
		{
			this.gameObject.transform.localScale = Vector3.zero;
		}

		//tileHitClip = Resources.Load<AudioClip>("Sound/Hero/Sound_PC_01_hit01");
	}

	//------------------------------------------------------------------------------------------------------------------

	public void AddTileHealth(int h)
	{
		soTileAttribute.currentTileHealth += h;
	}

	public void ReduceTileHealth(int h)
	{
		soTileAttribute.currentTileHealth -= h;
	}
	
	public bool IsReadyToDestroyTile(int _)
	{
		bool c = false;
		if (GetCurrentTileHealth() <= 0)
		{
			return true;
		}
	
		return c;
	}
	
	//------------------------------------------------------------------------------------------------------------------

	public void EnemySpawn()
	{
		
	}
	

	//------------------------------------------------------------------------------------------------------------------
	public void Hit(int reduceval)
	{
		if (soTileAttribute.isTileVisible)
		{
			ReduceTileHealth(reduceval);

			if (tileHitClip != null)
			{
				AudioSource.PlayClipAtPoint(tileHitClip,this.transform.position);
			}
			
		}
		
		this.transform.DOShakeScale(effectDuration, shakeStrength, shakeVibrato, shakeRandomness).OnComplete(() =>
		{
			if (IsReadyToDestroyTile(reduceval))
			{
				soTileAttribute.isTileVisible = false;
				DisableTile();
			}

		});

	}

	//------------------------------------------------------------------------------------------------------------------
	

	//------------------------------------------------------------------------------------------------------------------
	private void DisableTile()
	{
		Collider col = this.GetComponent<BoxCollider>();
		this.transform.DOScale(new Vector3(0f, 0f, 0f), .2f).SetEase(Ease.InQuad).OnComplete(() =>
		{
			if (col != null)
			{
				col.enabled = false;
			}
		});
		    
	}
	
	public void EnableTile()
	{
		this.transform.DOScale(new Vector3(1f, 1f, 1f), .1f).SetEase(Ease.InQuad);
	}
	//------------------------------------------------------------------------------------------------------------------

	public void TimeToEnable()
	{
		
	}

	//------------------------------------------------------------------------------------------------------------------
	void SetFloorTag()
	{
		// switch (floorTileType)
		// {
		// 	case FloorTileType.GrassTile:
		// 		this.gameObject.tag = "GrassTile";
		// 		break;
		// 	case FloorTileType.SandTile:
		// 		this.gameObject.tag = "SandTile";
		// 		break;
		// 	case FloorTileType.RockTile:
		// 		this.gameObject.tag = "RockTile";
		// 		break;
		// 	case FloorTileType.LavaTile:
		// 		this.gameObject.tag = "LavaTile";
		// 		break;
		// 	case FloorTileType.SnowTile:
		// 		this.gameObject.tag = "SnowTile";
		// 		break;
		// 	case FloorTileType.NonDestructable:
		// 		this.gameObject.tag = "NonDestructable";
		// 		break;
		// 	default:
		// 		throw new ArgumentOutOfRangeException();
		// }
	}

	//------------------------------------------------------------------------------------------------------------------

	public Transform GetTileTransform() => this.transform;
	public int GetCurrentTileHealth() => soTileAttribute.currentTileHealth;

	#endregion

    }
}


