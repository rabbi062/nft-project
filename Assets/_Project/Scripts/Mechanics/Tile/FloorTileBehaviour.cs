using System;
using DG.Tweening;
using UnityEngine;

namespace TD
{
    public class FloorTileBehaviour
    {
	#region Variable
	
	

	//-------------------------------------------------------------------------------------------------------------
	public static FloorTileBehaviour Instance
	{
		get { return CsSingleton<FloorTileBehaviour>.GetInstance(); }
	}


	#endregion

	#region Function

	//-------------------------------------------------------------------------------------------------------------
	public void FloorTileHitByHammer(GameObject obj, int reduceVal)
	{
		var tile = obj.GetComponent<TileBaseSc>();

		switch (tile.soTileAttribute.floorTileType)
		{
			case FloorTileType.GrassTile:
				tile.Hit(reduceVal);
				
				break;
			case FloorTileType.SandTile:
				tile.Hit(reduceVal);
				break;
			case FloorTileType.RockTile:
				tile.Hit(reduceVal);
				break;
			case FloorTileType.LavaTile:
				tile.Hit(reduceVal);
				break;
			case FloorTileType.SnowTile:
				tile.Hit(reduceVal);
				break;
			case FloorTileType.NonDestructable:
				tile.Hit(0);
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}


	}

	


	#endregion

    }
}


